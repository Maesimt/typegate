import { defineConfig } from 'vocs'
 
export default defineConfig({
  description: 'Build reliable apps & libraries with lightweight, \
    composable, and type-safe modules that interface with Ethereum.', 
  title: 'TypeGate',
  twoslash: { 
    compilerOptions: { 
      strict: true, 
    },
  },
  socials: [
    { 
      icon: 'github', 
      link: 'https://gitlab.com/Maesimt/typegate', 
    }
  ],
  sidebar: [
      { 
        text: 'API', 
        link: '/api', 
      }, 
      { 
        text: 'Guides', 
        collapsed: false, 
        items: [ 
          { 
            text: 'Getting started', 
            link: '/guides/getting-started', 
          },
          { 
            text: 'Result', 
            link: '/guides/result', 
          }, 
          { 
            text: 'Custom parsers', 
            link: '/guides/custom-parsers', 
          }, 
          { 
            text: 'Recursive types', 
            link: '/guides/recursive-types', 
          }, 
          { 
            text: 'Progressive integration', 
            link: '/guides/progressive-integration', 
          },
          { 
            text: 'Syncing huge types', 
            link: '/guides/syncing-huge-types', 
          }, 
        ], 
      },
      { 
        text: 'Challenges', 
        collapsed: false, 
        items: [
          { 
            text: 'Runtime speed', 
            link: '/challenges/runtime-speed', 
          }, 
          { 
            text: 'Enum equality', 
            link: '/challenges/enum-equality', 
          },
          { 
            text: 'Bundle size', 
            link: '/challenges/bundle-size', 
          }, 
          { 
            text: 'TSC load', 
            link: '/challenges/tsc-load', 
          }, 
        ], 
      }
  ]
})