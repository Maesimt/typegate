import * as React from 'react'

export default function Footer() {
  return (
    <div>
      <div>Released under the ISC License.</div>
      <div>Copyright © 2023-present <a href="https://www.linkedin.com/in/guillaume-cummings-81542a139/">Guillaume Cummings</a>.</div>
    </div>  
  )
}