import * as React from 'react'

export default function Logo({ children }: { children: React.ReactNode }) {
  return (
    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', width: "100%"}}>
      <img src="/typegate.png" style={{display: 'flex', textAlign: "center", width: "50%"}}/>
      <h1 style={{width: '200px', textAlign: 'center', fontSize: 'var(--vocs-fontSize_h1)'}}>Type<b>Gate</b></h1>
      <div style={{display: 'flex', marginTop: '25px', gap: '5px'}}>
        <a href="https://badge.fury.io/js/typegate"><img src="https://badge.fury.io/js/typegate.svg" alt="npm version" /></a>
        <a href="https://gitlab.com/Maesimt/typegate/-/raw/main/license?ref_type=heads"><img alt="license isc" src="https://img.shields.io/badge/License-ISC-blue.svg" /></a> 
        <a href="https://gitlab.com/Maesimt/typegate/-/commits/main"><img alt="coverage report" src="https://gitlab.com/Maesimt/typegate/badges/main/coverage.svg" /></a>
        <a href="https://gitlab.com/Maesimt/typegate/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/Maesimt/typegate/badges/main/pipeline.svg" /></a>
      </div>
    </div>
  );
}