import { describe, it, expect } from '@jest/globals'
import { number, literal, string, union, object, required, intersection, property } from "../src";
import { discriminatedUnion } from '../src/union';
import { Infer } from '../src/parser';


describe('union tests', () => {
  const cases = [
    { label: "union(string)", schema: union(string), value: "bonjour", shouldPass: true },
    { label: "union(string, number)", schema: union(string, number), value: 23, shouldPass: true },
    { label: "union(string, number, literal(true))", schema: union(string, number, literal(true)), value: true, shouldPass: true },
    { label: "union(literal('a')", schema: union(literal("a"), literal("b"), literal("c"), literal("d")), value: "d", shouldPass: true },
    { label: "union(string)", schema: union(string), value: null, shouldPass: false },
    { label: "union(string, number)", schema: union(string, number), value: false, shouldPass: false },
    { label: "union(string, number, literal(true))", schema: union(string, number, literal(true)), value: false, shouldPass: false },
    { label: "union(literal('a'), literal('b'), literal('c'), literal('d'))", schema: union(literal("a"), literal("b"), literal("c"), literal("d")), value: "e", shouldPass: false },
    { label: "union(number, string)", schema: union(number, string), value: false, shouldPass: false },
    { label: "union(number, string)", schema: union(number, string), value: null, shouldPass: false },
    { label: "union(number, string)", schema: union(number, string), value: "a", shouldPass: true },
    { label: "union(number, string)", schema: union(number, string), value: 42, shouldPass: true },
  ];

  it.each(cases)('Expected the schema $label to be $shouldPass given this value "$value."  ', ({ schema, value, shouldPass }) => {
    expect(schema.parse(value).success).toEqual(shouldPass);
  });

  it('Good error message as a standalone schema', () => {
    const person = object(property("name", string)).setTypeName("Person");
    const car = object(property("wheels", number)).setTypeName("Car");

    const schema = union(person, car).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      expect(result.error).toBe("Failed to parse a Thing. We received a null but it was supposed to one of (Person | Car).");
    } else {
      expect(false).toBe(true);
    }
  });

  it('Good error message while embedded in an object', () => {
    const person = object(property("name", string)).setTypeName("Person");
    const car = object(property("wheels", number)).setTypeName("Car");

    const thing = union(person, car).setTypeName("Thing");
    const schema = object(property("thing", thing));

    const result = schema.parse({ "thing": 123 });

    if (!result.success) {
      expect(result.error).toBe("Failed to parse an object. We received a number at \"<root>.thing\" but it was supposed to one of (Person | Car).");
    } else {
      expect(false).toBe(true);
    }
  });
});

describe('discriminated Union Tests', () => {
  const person = object(
    property("tag", literal(666)),
    property("name", string),
  ).setTypeName("Person");

  const car = object(
    property("tag", literal("car")),
    property("wheels", number),
  ).setTypeName("Car");

  const schema = discriminatedUnion("tag", person, car).setTypeName("Thing");

  it('Pivot on the type properly', () => {
    const result = schema.parse(null);

    if (!result.success) {
      // This test is not about the runtime expectation. It's about validating the types.
      return;
    }

    const thing = result.value;

    if (thing.tag === 666){
      thing.name;
      const personInstance: Infer<typeof person> = thing;
    } else if (thing.tag === "car") {
      thing.wheels;
      const carInstance: Infer<typeof car> = thing;
    } else {
      const neverCheck: never = thing;
    }
  });

  it('Pivot on the type properly even with intersection.', () => {
    const base = object(property("age", number));

    const person = intersection(
      base,
      object(
        property("tag", literal(666)),
        property("name", string),
      ),
    ).setTypeName("Person");
  
    const car = intersection(
      base,
      object(
        property("tag", literal("car")),
        property("wheels", number),
      ),
    ).setTypeName("Car");
  
    const schema = discriminatedUnion("tag", person, car).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      // This test is not about the runtime expectation. It's about validating the types.
      return;
    }

    const thing = result.value;

    if (thing.tag === 666){
      thing.name;
      const personInstance: Infer<typeof person> = thing;
    } else if (thing.tag === "car") {
      thing.wheels;
      const carInstance: Infer<typeof car> = thing;
    } else {
      const neverCheck: never = thing;
    }
  });

  it('Given the discriminated key is missing on the object expect an error', () => {
    const result = schema.parse({ name: "Mike" });

    if (result.success) {
      expect(false).toBe(true);
      return;
    }

    expect(result.error).toBe("The Thing is missing the required property \"tag\".");
  });

  it('Given the discriminated value is missing in the union definition expect an error', () => {
    const result = schema.parse({ tag: "alien" });

    if (result.success) {
      expect(false).toBe(true);
      return;
    }

    expect(result.error).toBe("Failed to parse a Thing. We received an object but it was supposed to one of (Person | Car).");
  });



  describe('When the discriminated value exists in the union', () => {
    it('and the value passes the parsing expect success', () => {
      const result = schema.parse({ tag: 666, name: "Mike" });

      if (!result.success) {
        expect(false).toBe(true);
        return;
      }

      expect(result.value).toEqual({ tag: 666, name: "Mike" });
    });

    it('and the value fails the parsing expect failure with message', () => {
      const result = schema.parse({ tag: 666 });

      if (result.success) {
        expect(false).toBe(true);
        return;
      }

      expect(result.error).toBe("The Person is missing the required property \"name\".");
    });

    it('given each member is an intersection and and the appropriate member schema fails expects failure with message', () => {
      const base = object(property("age", number));

      const person = intersection(
        base,
        object(
          property("tag", literal(666)),
          property("name", string),
        ),
      ).setTypeName("Person");
    
      const car = intersection(
        base,
        object(
          property("tag", literal("car")),
          property("wheels", number),
        ),
      ).setTypeName("Car");

      const schema = discriminatedUnion("tag", person, car).setTypeName("Thing");

      const result = schema.parse({ tag: 666, age: 22 });

      if (result.success) {
        expect(false).toBe(true);
        return;
      }

      expect(result.error).toBe("The Person is missing the required property \"name\".");
    });
  })

});

it('Swag', () => {
  const extremeUnion = union(
    literal("A"),
    literal("B"),
    literal("C"),
    literal("D"),
    literal("E"),
    literal("F"),
    literal("G"),
    literal("H"),
    literal("I"),
    literal("J"),
    literal("K"),
    literal("K"),
    literal("L"),
    literal("M"),
    literal("N"),
    literal("O"),
    literal("P"),
    literal("Q"),
    literal("R"),
    literal("S"),
    literal("T"),
    literal("Y"),
    literal("U"),
    literal("V"),
    literal("X"),
    literal("Y"),
    literal("Z"),
    literal("AA"),
    literal("BB"),
    literal("CC"),
    literal("DD"),
    literal("EE"),
    literal("FF"),
    literal("GG"),
    literal("HH"),
    literal("II"),
    literal("JJ"),
    literal("KK"),
    literal("KK"),
    literal("LL"),
    literal("MM"),
    literal("NN"),
    literal("OO"),
    literal("PP"),
    literal("QQ"),
    literal("RR"),
    literal("SS"),
    literal("TT"),
    literal("YY"),
    literal("UU"),
    literal("VV"),
    literal("XX"),
    literal("YY"),
    literal("ZZ"),
    literal("AAA"),
    literal("BBB"),
    literal("CCC"),
    literal("DDD"),
    literal("EEE"),
    literal("FFF"),
    literal("GGG"),
    literal("HHH"),
    literal("III"),
    literal("JJJ"),
    literal("KKK"),
    literal("LLL"),
    literal("MMM"),
    literal("NNN"),
    literal("OOO"),
    literal("PPP"),
    literal("QQQ"),
    literal("RRR"),
    literal("SSS"),
    literal("TTT"),
    literal("YYY"),
    literal("UUU"),
    literal("VVV"),
    literal("XXX"),
    literal("YYY"),
    literal("ZZZ"),
  );

  type ExtremeUnion = Infer<typeof extremeUnion>;
});


