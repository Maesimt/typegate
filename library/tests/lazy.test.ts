import { describe, it, expect } from '@jest/globals';

import { t } from "../src";
describe('lazy test for recursive schema', () => {
  it('auto-complete works with an annotation passed to the lazy.', () => {
    expect.assertions(1);

    type Person = {
      name: string;
      father?: Person;
    };

    const person = t.object(
      t.property('name', t.string),
      t.optionalProperty('father', t.lazy<Person>(() => person))
    );

    const result = person.parse({
      name: 'Mike',
      father: {
        name: 'Jim',
        father: {
          name: 'Henry'
        }
      }
    });

    if (result.success) {
      expect(result.value.father?.father?.name).toEqual('Henry');
    }
  });

  it('Given a nested recursive property is missing gets an error.', () => {
    expect.assertions(1);

    type Person = {
      name: string;
      father?: Person;
    };

    const person = t.object(
      t.property('name', t.string),
      t.optionalProperty('father', t.lazy<Person>(() => person))
    )

    const result = person.parse({
      name: 'Mike',
      father: {
        name: 'Jim',
        father: {
          // name: 'Henry', <--- This mandatory property is missing.
          father: {
            name: 'Bob'
          }
        }
      }
    });

    if (!result.success) {
      expect(result.error).toEqual("The object is missing the required property \"name\" in \"<root>.father.father\".");
    }
  })

})