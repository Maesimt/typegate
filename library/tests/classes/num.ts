import { Result, failure, success, CustomParsingError, ParsingError, number } from "../../src";
import { Schema, createSchema } from "../../src/parser";

export class NotANumber extends Error { readonly tag = 'NotANumber' }

export type NumError = NotANumber;

export class Num {
  // when dividing by zero you get a NaN,
  // not an exception. So you are never garantied
  // to have a Number after doing some math in JS.
  // This class is there to tell the programmer that
  // we are sure that there's a number in it.
  readonly value: number;

  private constructor(value: number) {
    this.value = value;
  }

  static from(value: number): Result<NumError, Num> {
    if (isNaN(value)) {
      return failure(new NotANumber());
    }

    return success(new Num(value));
  }
}

export const num: Schema<Num> = createSchema((value: unknown): Result<ParsingError, Num> => {
  return number.parse(value)
    .andThen(Num.from)
    .mapError(() => CustomParsingError.from("num", value));
});