import { CustomParsingError, ParsingError, Result, failure, success } from "../../src";
import { Schema, createSchema } from "../../src/parser";
import { Num, num } from "./num";

class CantBeNegative extends Error {
  private readonly __class = Symbol.for('CantBeNegative');
  readonly __tag = 'CantBeNegative';
}

export class PositiveNumber {
  private readonly __class = Symbol.for('PositiveNumber');
  readonly __tag = 'PositiveNumber';
  private constructor(readonly value: number) { }

  static from(number: Num): Result<CantBeNegative, PositiveNumber> {
    if (number.value < 0) {
      return failure(new CantBeNegative());
    }

    return success(new PositiveNumber(number.value));
  }
}

export const positiveNumber: Schema<PositiveNumber> = createSchema((value: unknown): Result<ParsingError, PositiveNumber> => {
  return num.parse(value)
    .andThen(PositiveNumber.from)
    .mapError((e) => CustomParsingError.from("positive number", value));
});