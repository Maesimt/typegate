import { string, CustomParsingError, ParsingError, Result, failure, success } from "../../src";
import { Schema, createSchema } from "../../src/parser";

export class StringIsEmpty extends Error { readonly tag = 'StringIsEmpty' };

export type NonEmptyStringError = StringIsEmpty;

export class NonEmptyString {
  readonly tag = 'NonEmptyString';
  readonly value: string;

  private constructor(text: string) {
    this.value = text;
  }

  static from(text: string): Result<NonEmptyStringError, NonEmptyString> {
    if (text.length < 1) {
      return failure(new StringIsEmpty());
    }

    return success(new NonEmptyString(text));
  }
}

export const nonEmptyString: Schema<NonEmptyString> = createSchema((value: unknown): Result<ParsingError, NonEmptyString> => {
  return string.parse(value)
    .andThen(NonEmptyString.from)
    .mapError((e) => CustomParsingError.from("non empty string", value));
});