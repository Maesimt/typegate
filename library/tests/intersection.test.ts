import { intersection, number, object, property, string } from "../src";

const withName = object(property('name', string));
const withAge = object(property('age', number));

describe('intersection tests', () => {
  const cases = [
  [1, intersection(withName, withAge), { name: 'mike', age: 24 }, true],
    [2, intersection(withName, withAge), { name: 'mike' }, false],
    [3, intersection(withName, withAge), { age: 24 }, false],
    [4, intersection(withName, withAge), {}, false],
  ] as const;

  it.each(cases)('Case %i', (caseNumber, parser, data, shouldPass) => {
    expect(parser.parse(data).success).toEqual(shouldPass);
  });

  it('has a good error message', () => {
    const schema = intersection(withName, withAge).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      expect(result.error).toBe("Expected a Thing but it was a null.");
    } else {
      expect(false).toBe(true);
    }
  });
});