import { number, string, tuple, boolean } from "../src";

describe('tuple tests', () => {
  const workingCases = [
    { label: "tuple(string)", schema: tuple(string), value: ["abc"], shouldPass: true },
    { label: "tuple(string, string)", schema: tuple(string, string), value: ["abc", "abc"], shouldPass: true },
    { label: "tuple(string, string, string)", schema: tuple(string, string, string), value: ["abc", "abc", "abc"], shouldPass: true },
    { label: "tuple(number, string, boolean)", schema: tuple(number, string, boolean), value: [123, "abc", false], shouldPass: true },
  ];

  const failingCases = [
    { label: "tuple(string)", schema: tuple(string), value: 123, shouldPass: false },
    { label: "tuple(string)", schema: tuple(string), value: [], shouldPass: false },
    { label: "tuple(string, string)", schema: tuple(string, string), value: ["abc", true], shouldPass: false },
    { label: "tuple(string, string, string)", schema: tuple(string, string, string), value: ["abc", 123, "abc"], shouldPass: false },
    { label: "tuple(number, string, boolean)", schema: tuple(number, string, boolean), value: [123, "abc"], shouldPass: false },
  ];

  it.each([...workingCases, ...failingCases])('Expected the parser $label to be $shouldPass given this value "$value."  ', ({ schema, value, shouldPass }) => {
    expect(schema.parse(value).success).toEqual(shouldPass);
  });

  describe('messages', () => {
    it("Given it's not a fixed array expect the correct message.", () => {
      expect.assertions(1);

      const result = tuple(string, number).parse("abc");

      if (!result.success) {
        expect(result.error).toEqual("Expected a tuple but it was a string.");
      }
    });

    it("Given it's a fixed array but the length is incorrect expect the correct message.", () => {
      expect.assertions(1);

      const result = tuple(string, number).parse(["abc"]);

      if (!result.success) {
        expect(result.error).toEqual("Expected a tuple and the length was supposed to be 2 but it was 1.");
      }
    });

    it("Given it's a fixed array with the correct length but one element has the wrong type expect the correct message.", () => {
      expect.assertions(1);

      const result = tuple(string, number).parse(["abc", "def"]);

      if (!result.success) {
        expect(result.error).toEqual("Expected a tuple and the value at \"[1]\" was supposed to be a number but it was a string.");
      }
    })
  });
});