import { describe, it, expect } from '@jest/globals'
import { array } from "../src/array";
import { string } from "../src/primitives";

describe('Array test', () => {

  it('given something that is not an array expect an error', () => {
    expect.assertions(1);
    const schema = array(string);

    const result = schema.debug(12);

    if (!result.success) {
      expect(result.error.tag).toEqual("BasicParsingError");
    }
  });

  it('given an empty array expect a success', () => {
    expect.assertions(1);
    const schema = array(string);

    const result = schema.parse([]);

    if (result.success) {
      expect(result.value).toEqual([]);
    }
  });

  it('given three elements that are valid expect a success', () => {
    expect.assertions(1);
    const schema = array(string);

    const result = schema.parse(["abc", "def", "ghi"]).map(e => e.map(e => e));

    if (result.success) {
      expect(result.value).toEqual(["abc", "def", "ghi"]);
    }
  });

  it('given the first element is wrong expect an error', () => {
    expect.assertions(1);
    const schema = array(string);

    const result = schema.parse([123, "def", "ghi"]);

    if (!result.success) {
      expect(result.error).toEqual("Expected an array and the value at \"[0]\" was supposed to be a string but it was a number.");
    }
  });

  it('given an element in the middle is wrong expect an error', () => {
    expect.assertions(1);
    const schema = array(string);

    const result = schema.parse(["abc", 123, "ghi"]);

    if (!result.success) {
      expect(result.error).toEqual("Expected an array and the value at \"[1]\" was supposed to be a string but it was a number.");
    }
  });

  it('given the last element is wrong expect an error', () => {
    expect.assertions(1);
    const schema = array(string);

    const result = schema.parse(["abc", "def", 123]);

    if (!result.success) {
      expect(result.error).toEqual("Expected an array and the value at \"[2]\" was supposed to be a string but it was a number.");
    }
  });
})