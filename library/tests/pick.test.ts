import {  number, object, optionalProperty, partial, pick, property, string } from "../src";

const person = object(
  property('name', string),
  optionalProperty('age', number)
);

describe('intersection tests', () => {
  const cases = [
    [1, pick(person, ['name', 'age']), { name: 'mike', age: 24 }, true],
    [2, pick(person, ['name']), { name: 'mike' }, true],
    [3, pick(person, ['age']), { age: 24 }, true],
    [4, pick(person, []), {}, true],
    [5, pick(person, ['name']), null, false],
  ] as const;

  it.each(cases)('Case %i', (caseNumber, parser, data, shouldPass) => {
    expect(parser.parse(data).success).toEqual(shouldPass);
  });

  it('has a good error message', () => {
    const schema = partial(person).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      expect(result.error).toBe("Expected a Thing but it was a null.");
    } else {
      expect(false).toBe(true);
    }
  });
});