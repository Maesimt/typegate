import { describe, it, expect } from '@jest/globals'
import { string, number,  object, optionalProperty, required, property, Infer, array } from '../src';

describe('object parsing test', () => {
  it('given all properties are optional expect an empty object to pass', () => {
    expect.assertions(1);

    const parser = object(
      optionalProperty('name', string),
      optionalProperty('age', number),
    );

    const result = parser.parse({});

    if (result.success) {
      expect(result.value).toEqual({});
    }
  });

  it('given all required properties are present expects parser to pass.', () => {
    expect.assertions(1);

    const parser = object(
      property('name', string),
      property('age', number),
    );

    const result = parser.parse({ name: 'John', age: 14 });

    if (result.success) {
      expect(result.value).toEqual({ name: 'John', age: 14 });
    }
  });

  it('given one required property is missing expects parser to fail.', () => {
    expect.assertions(1);

    const parser = object(
      property('name', string),
      property('age', number),
    );

    const result = parser.parse({ age: 14 });

    if (!result.success) {
      expect(result.error).toEqual("The object is missing the required property \"name\".");
    }
  });

  it('given the data is not an object expects parser to fail.', () => {
    expect.assertions(1);

    const parser = object(
      property('name', string),
      property('age', number),
    );

    const result = parser.parse(123);

    if (!result.success) {
      expect(result.error).toEqual("Expected an object but it was a number.");
    }
  });

  it('given the nested data is not an object expects parser to fail.', () => {
    expect.assertions(1);

    const parser = object(
      property('name', string),
      property('age', number),
      property('file', object(
        property('id', string)
      ))
    );

    const result = parser.parse({
      name: 'Bob',
      age: 34,
      file: 123,
    });

    if (!result.success) {
      expect(result.error).toEqual("Expected an object and the value at \"<root>.file\" was supposed to be an object but it was a number.");
    }
  });

  it('given the nested data property is wrong expects parser to fail.', () => {
    expect.assertions(1);

    const parser = object(
      property('name', string),
      property('age', number),
      property('file', object(
        property('id', string)
      ))
    );

    const result = parser.parse({
      name: 'Bob',
      age: 34,
      file: {
        id: 123
      },
    });

    if (!result.success) {
      expect(result.error).toEqual("Expected an object and the value at \"<root>.file.id\" was supposed to be a string but it was a number.");
    }
  });

  it('given the deeply nested data property is missing expects parser to fail.', () => {
    expect.assertions(1);

    const parser = object(
      property('name', string),
      property('age', number),
      property('file', object(
        property('id', string),
        property('agent', object(
          property('name', string)
        ))
      ))
    );

    // TODO recursively exploded the nested parsers (object of object of object)

    const result = parser.parse({
      name: 'Bob',
      age: 34,
      file: {
        id: 'abc-123',
        agent: {},
      },
    });

    if (!result.success) {
      expect(result.error).toEqual("The object is missing the required property \"name\" in \"<root>.file.agent\".");
    }
  });


  it('given "createNewObjects" enabled object should not have extra keys', () => {
    expect.assertions(1);

    const schema = object(
      property("name", string),
      property("age", number),
      property("kids", array(object(
        property("name", string)
      ))))
    .setTypeName("person");

    const result = schema.parse({
      name: "bob",
      age: 32,
      gender: 'male',
      kids: [
        { name: "Mike", age: 7},
        { name: "Chloe", age: 3},
      ]
    }, {mode: 'createNewObjects'}).unsafe();

    expect(result).toStrictEqual({
      name: "bob",
      age: 32,
      kids: [
        {name: "Mike"}, 
        {name: "Chloe"}
      ]});
  });
});

describe('required parsing test', () => {
  it('given something is not an object expect an error', () => {
    expect.assertions(1);
    const parser = object(
      property('name', string),
    );

    const result = parser.parse(null);

    if (!result.success) {
      expect(result.error).toEqual("Expected an object but it was a null.");
    }
  });

});

describe('optional parsing test', () => {
  it('given something is not an object expect an error', () => {
    expect.assertions(1);
    const parser = object(
      optionalProperty('name', string)
    );

    const result = parser.parse(null);

    if (!result.success) {
      expect(result.error).toEqual("Expected an object but it was a null.");
    }
  });
});



it('works for deeply nested objects', () => {
  const schema = object(
    property("a", object(
      property("b", object(
        property("c", object(
          property("d", object(
            property("e", object(
              property("f", object(
                property("g", object(
                  property("h", object(
                    property("i", object(
                      property("j", object(
                        property("k", object(
                          property("l", object(
                            property("m", object(
                              property("n", object(
                                property("o", object(
                                  property("p", object(
                                    property("q", object(
                                      property("r", object(
                                        property("s", object(
                                          property("t", object(
                                            property("u", object(
                                              property("v", object(
                                                property("w", object(
                                                  property("x", object(
                                                    property("y", object(
                                                      property("z", object())
                                                    ))
                                                  ))
                                                ))
                                              ))
                                            ))
                                          ))
                                        ))
                                      ))
                                    ))
                                  ))
                                ))
                              ))
                            ))
                          ))
                        ))
                      ))
                    ))
                  ))
                ))
              ))
            ))
          ))
        ))
      ))
    ))
  )

  type Test = Infer<typeof schema>

  expect(true).toBe(true);
});
