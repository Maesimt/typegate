import { describe, it, expect } from '@jest/globals'
import { string, number,  object, optionalProperty, record, property, Infer, array, boolean, tuple, union, literal, tsEnum } from '../src';

describe('record parsing test', () => {
  describe('Check that the object received is compatible with record.', () => {

    it('Given a record with a few valid key-value expect a success', () => {
      expect.assertions(1);
  
      const parser = record(string, boolean);
  
      const result = parser.parse({abc: true, def: false});
  
      if (result.success) {
        expect(result.value).toEqual({abc: true, def: false});
      }
    });
  
    it('Given a record with no key-value pair expect a success', () => {
      expect.assertions(1);
  
      const parser = record(string, boolean);
  
      const result = parser.parse({});
  
      if (result.success) {
        expect(result.value).toEqual({});
      }
    });
  
    it('Given an array expect an error', () => {
      expect.assertions(1);
  
      const parser = record(string, boolean).setTypeName("FlagRecord");
  
      const result = parser.parse([]);
  
      if (!result.success) {
        expect(result.error).toEqual("Expected a FlagRecord but it was an array.");
      }
    });
  
    it('Given an array expect an error (without type name set)', () => {
      expect.assertions(1);
  
      const parser = record(string, boolean);
  
      const result = parser.parse([]);
  
      if (!result.success) {
        expect(result.error).toEqual("Expected a record but it was an array.");
      }
    });
  });
  describe('Check that all the keys are valid', () => {
    it('Works with enum', () => {
      expect.assertions(1);

      enum Color {
        red = "red",
        blue = "blue",
      }
  
      const parser = record(tsEnum(Color), boolean);
  
      // TODO: Add a type validation to make sure the keys are restricted to union member at the type level.
      const result = parser.parse({[Color.red]: true, [Color.blue]: true});
  
      expect(result.success).toBe(true);
    });

    it('Works with literal', () => {
      expect.assertions(1);
  
      const parser = record(literal('allo'), boolean);
  
      // TODO: Add a type validation to make sure the keys are restricted to union member at the type level.
      const result = parser.parse({"allo": true});
  
      expect(result.success).toBe(true);
    });

    it('Works with union', () => {
      expect.assertions(1);
  
      const parser = record(union(literal("abc"), literal("def")), boolean);
  
      // TODO: Add a type validation to make sure the keys are restricted to union member at the type level.
      const result = parser.parse({"abc": true, def: true});
  
      expect(result.success).toBe(true);
    });
  })
});