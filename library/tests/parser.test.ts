import { describe, it, expect } from '@jest/globals'
import { array,  union, literal, boolean, object, required, property } from "../src";
import { NonEmptyString, nonEmptyString } from "./classes/nonEmptyString";
import { PositiveNumber, positiveNumber } from "./classes/positiveNumber";
import { Schema } from '../src/parser';

describe('Schema tests', () => {
  it('error messages for complex scenarios', () => {
    expect.assertions(1);

    type Kid = { name: NonEmptyString, music: 'dubstep' | 'classic' };
    type Person = { name: NonEmptyString, age: PositiveNumber, children: Kid[] }
    type File = { name: NonEmptyString, persons: Person[] }

    const kid: Schema<Kid> = object(
      property("name", nonEmptyString),
      property("music", union(literal('dubstep'), literal('classic')))
    ).setTypeName("Kid");


    const person: Schema<Person> = object(
      property("name", nonEmptyString),
      property("age", positiveNumber),
      property("children", array(kid))
    ).setTypeName("Person");


    const file: Schema<File> = object(
      property("name", nonEmptyString),
      property("persons", array(person))
    ).setTypeName("File");


    const payload = {
      name: "TaxReturn",
      persons: [
        { name: "Mike", age: 39, children: [{ name: "Jim", music: 'classic' }] },
        { name: "Clara", age: 34, children: [{ name: "bob", music: 123 }] },
      ]
    }

    const result = file.parse(payload);

    if (!result.success) {
      expect(result.error).toBe("Failed to parse a File. We received a number at \"<root>.persons[1].children[0].music\" but it was supposed to one of (\"dubstep\" | \"classic\").");
    }
  });

  it('given the input is a class that does not pass the parser expect a message that says it is a class.', () => {
    expect.assertions(1);
    class Bob {
      sayHi() { return "Hi!"; }
    };
    const input = new Bob();
    const parser = boolean;

    const result = parser.parse(input);

    if (!result.success) {
      expect(result.error).toEqual("Expected a boolean but it was a Bob class instance.");
    }
  });

  it('given the input is an object that does not pass the parser expect a message that says it is an object.', () => {
    expect.assertions(1);
    const parser = boolean;

    const result = parser.parse({});

    if (!result.success) {
      expect(result.error).toEqual("Expected a boolean but it was an object.");
    }
  });
});