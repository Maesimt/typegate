import {  number, object, omit, partial, property, string } from "../src";

const person = object(
  property('name', string),
  property('age', number)
);

describe('intersection tests', () => {
  const cases = [
    [1, omit(person, []), { name: 'mike', age: 24 }, true],
    [2, omit(person, ['age']), { name: 'mike' }, true],
    [3, omit(person, ['name']), { age: 24 }, true],
    [4, omit(person, ['name', 'age']), {}, true],
    [5, omit(person, ['age']), null, false],
  ] as const;

  it.each(cases)('Case %i', (caseNumber, parser, data, shouldPass) => {
    expect(parser.parse(data).success).toEqual(shouldPass);
  });

  it('has a good error message', () => {
    const schema = partial(person).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      expect(result.error).toBe("Expected a Thing but it was a null.");
    } else {
      expect(false).toBe(true);
    }
  });
});