import { describe, it, expect } from '@jest/globals'
import { failure, map, success } from "../src";


describe("Testing Result behavior", () => {
  describe("Smart constructors", () => {
    it("expect .success() to create a success", () => {
      const actual = success(123);

      expect(actual.success).toBe(true);
    });

    it("expect .failure() to create a failure", () => {
      const actual = failure(123);

      expect(actual.success).toBe(false);
    });
  });

  describe("Given a success", () => {
    it("expects value to be available", () => {
      const actual = success(123);

      let unwrap;
      if (actual.success) {
        unwrap = actual.value;
      }

      expect(unwrap).toBe(123);
    });

    it("expects isSuccess to be true", () => {
      const actual = success(123);

      expect(actual.success).toBe(true);
    });

    it("expects map to apply the function on the success value", () => {
      const actual = success(2);

      const result = actual.map((element) => element * 2);

      let value;

      if (result.success) {
        value = result.value;
      }

      expect(value).toBe(4);
    });

    it("expects mapError NOT to apply the function to the error", () => {
      const actual = success(2);
      const someFunction = jest.fn();

      actual.mapError(someFunction);

      expect(someFunction.mock.calls).toHaveLength(0);
    });

    it("expects andThen to apply the function on the success value", () => {
      const actual = success(2);

      const result = actual.andThen(() => success("win"));

      let value;

      if (result.success) {
        value = result.value;
      }

      expect(value).toBe("win");
    });

    it("expects orElse NOT to apply the function on the success value", () => {
      const actual = success(2);
      const someFunction = jest.fn();

      actual.orElse(someFunction);

      expect(someFunction.mock.calls).toHaveLength(0);
    });

    it("expects withDefault to grab the value from the success", () => {
      const actual = success(2);

      const result = actual.withDefault(0);

      expect(result).toBe(2);
    });
  });

  describe("Given a failure", () => {
    it("expects error to be available", () => {
      const actual = failure(404);

      let error;

      if (!actual.success) {
        error = actual.error;
      }

      expect(error).toBe(404);
    });

    it("expects success to be false", () => {
      const actual = failure(404);

      expect(actual.success).toBe(false);
    });

    it("expects map NOT to apply the function on the success value", () => {
      const actual = failure(404);
      const someFunction = jest.fn();

      actual.map(someFunction);

      expect(someFunction.mock.calls).toHaveLength(0);
    });

    it("expects mapError to apply the function to the error", () => {
      const actual = failure(404);

      const result = actual.mapError((element) => element * 2);

      let error;

      if (!result.success) {
        error = result.error;
      }

      expect(error).toBe(808);
    });

    it("expects andThen NOT to apply the function on the success value", () => {
      const actual = failure(404);
      const someFunction = jest.fn();

      actual.andThen(someFunction);

      expect(someFunction.mock.calls).toHaveLength(0);
    });

    it("expects orElse to apply the function on the error value.", () => {
      const actual = failure(404);

      const result = actual.orElse((element) => failure(element * 2));

      let error;

      if (!result.success) {
        error = result.error;
      }
      expect(error).toBe(808);
    });

    it("expects withDefault to return the default value", () => {
      const actual = failure(404);

      const error = actual.withDefault("fallback");

      expect(error).toBe("fallback");
    });
  });

  describe("Complex chaining", () => {
    it("expects everthing to chain nicely", () => {
      const result = success(2)
        .map((element) => element * 2)
        .mapError(() => "actual Nice error message")
        .andThen((value) => {
          if (value === 4) {
            return failure("Value was 4");
          }
          return success("Random");
        });

      let error;

      if (!result.success) {
        error = result.error;
      }

      expect(error).toBe("Value was 4");
    });

    it("expect success value to be accessible outside of type predicate branch", () => {
      const result = success(2);

      if (result.success) {
        // The test is able to access value of result;
        result.value;
      } else {
        // The test is able to access error of result;
        result.error;
      }

      expect(true).toBe(true);
    });
  });

  describe("map N", () => {
    it('should work with function of 1 arg', () => {
      expect.assertions(1);
      const constructor = (a: number) => a * 2;

      const result = map(constructor)(success(2))

      if (result.success) {
        expect(result.value).toEqual(4);
      }
    });

    it('should work with function of 3 arg', () => {
      expect.assertions(1);
      const constructor = (a: number, b: number, c: number) => a + b + c;

      const result = map(constructor)(success(2), success(4), success(4))

      if (result.success) {
        expect(result.value).toEqual(10);
      }
    });

    it('should work with function of 3 arg', () => {
      expect.assertions(1);
      const constructor = (a: number, b: number, c: number) => a + b + c;

      const result = map(constructor)(success(2), success(4), success(4))

      if (result.success) {
        expect(result.value).toEqual(10);
      }
    });

    it('given the last arg is a failure expect that error.', () => {
      expect.assertions(1);
      const constructor = (a: number, b: number, c: number) => a + b + c;

      const result = map(constructor)(success(2), success(4), failure("Boum"))

      if (!result.success) {
        expect(result.error).toEqual("Boum");
      }
    });

    it('given two args are failure return only the first one.', () => {
      expect.assertions(1);
      const constructor = (a: number, b: number, c: number) => a + b + c;

      const result = map(constructor)(failure("Early" as const), success(4), failure("Boum" as const))

      if (!result.success) {
        expect(result.error).toEqual("Early");
      }
    });

    it('given the caller is trying to map over something that is not a success nor a failure expects an exception.', () => {
      expect.assertions(1);
      const input = 666;
      const constructor = (a: number) => a;

      // Like if someone force typescript's had with as any.
      try {
        map(constructor)(input as any);
      } catch (e) {
        if (e instanceof Error) {
          expect(e.message).toEqual("Result.map did not find any success or error. This is not supposed to happen.");
        }
      }
    });
  });

  describe('unsafe', () => {
    it('given we call unsafe on a failure we get an exception.', () => {
      const result = success("abc").unsafe();

      expect(result).toEqual("abc");
    });

    it('given we call unsafe on a success we get the value unwrapped.', () => {
      expect.assertions(1);

      try {
        failure("error").unsafe();
      } catch (e) {
        if (e instanceof Error) {
          expect(e.message).toEqual("Could not unwrap the result because it is a failure.");
        }
      }
    });
  });
});
