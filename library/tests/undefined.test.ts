import { undefined as undefinedschema } from "../src";

describe('undefined tests', () => {
  const workingCases = [
    { label: "undefined", schema: undefinedschema, value: undefined, shouldPass: true },

  ];

  const failingCases = [
    { label: "null", schema: undefinedschema, value: null, shouldPass: false },
    { label: "123", schema: undefinedschema, value: 123, shouldPass: false },
    { label: "'abc'", schema: undefinedschema, value: 'abc', shouldPass: false },
    { label: "''", schema: undefinedschema, value: '', shouldPass: false },
    { label: "[1,2,3]", schema: undefinedschema, value: [1, 2, 3], shouldPass: false },
    { label: "[]", schema: undefinedschema, value: [], shouldPass: false },
    { label: "{}", schema: undefinedschema, value: {}, shouldPass: false },
    { label: "true", schema: undefinedschema, value: true, shouldPass: false },
    { label: "false", schema: undefinedschema, value: false, shouldPass: false },
  ];

  it.each([...workingCases, ...failingCases])('Expected the schema $label to be $shouldPass given this value "$value."  ', ({ schema, value, shouldPass }) => {
    expect(schema.parse(value).success).toEqual(shouldPass);
  });
});