import { tsEnum } from "../src";

describe('tsEnum tests', () => {
  describe('integer based enum', () => {
    enum Color { red, blue };
    const workingCases = [
      { label: "Color.blue", schema: tsEnum(Color), value: Color.blue, shouldPass: true },
      { label: "0", schema: tsEnum(Color), value: 0, shouldPass: true },
      { label: "1", schema: tsEnum(Color), value: 1, shouldPass: true },

    ];

    const failingCases = [
      { label: "2", schema: tsEnum(Color), value: 2, shouldPass: false },
      { label: '"red"', schema: tsEnum(Color), value: "red", shouldPass: false },
      { label: '"blue"', schema: tsEnum(Color), value: "blue", shouldPass: false },
      { label: 'true', schema: tsEnum(Color), value: true, shouldPass: false },
      { label: 'null', schema: tsEnum(Color), value: null, shouldPass: false },
      { label: 'undefined', schema: tsEnum(Color), value: undefined, shouldPass: false },
    ];

    it.each([...workingCases, ...failingCases])('Expected the schema $label to be $shouldPass given this value "$value."  ', ({ schema, value, shouldPass }) => {
      expect(schema.parse(value).success).toEqual(shouldPass);
    });

    it('infer a tyepof Color', () => {
      // If this line does not compile, tsEnum output type is too wide.
      let parsedValue: Color = tsEnum(Color).parse(Color.red).unsafe();
    });
  });

  describe('string based enum', () => {
    enum Color { red = "Redish", blue = "Blueish" };
    const workingCases = [
      { label: "Color.red", schema: tsEnum(Color), value: Color.red, shouldPass: true },
      { label: "Color.blue", schema: tsEnum(Color), value: Color.blue, shouldPass: true },
      { label: '"Redish', schema: tsEnum(Color), value: "Redish", shouldPass: true },
      { label: '"Blueish', schema: tsEnum(Color), value: "Blueish", shouldPass: true },

    ];

    const failingCases = [
      { label: "red", schema: tsEnum(Color), value: "red", shouldPass: false },
      { label: "blue", schema: tsEnum(Color), value: "blue", shouldPass: false },
      { label: "Grey", schema: tsEnum(Color), value: "Grey", shouldPass: false },
      { label: "0", schema: tsEnum(Color), value: "0", shouldPass: false },
      { label: '"0"', schema: tsEnum(Color), value: 0, shouldPass: false },
      { label: 'true', schema: tsEnum(Color), value: true, shouldPass: false },
      { label: 'null', schema: tsEnum(Color), value: null, shouldPass: false },
      { label: 'undefined', schema: tsEnum(Color), value: undefined, shouldPass: false },
    ];

    it.each([...workingCases, ...failingCases])('Expected the schema $label to be $shouldPass given this value "$value."  ', ({ schema, value, shouldPass }) => {
      expect(schema.parse(value).success).toEqual(shouldPass);
    });

    it('infer a Color value', () => {
      // If this line does not compile, tsEnum output type is too wide.
      let parsedValue: Color = tsEnum(Color).parse(Color.red).unsafe();
    });
  });

  describe('Readable error message', () => {
    it('works for integer enum', () => {
      enum Color { red, blue };
      const schema = tsEnum(Color);

      const result = schema.parse(null);

      if (result.success) {
        expect(false).toBe(true);
      } else {
        expect(result.error).toBe("Expected an enum member. It was supposed to be one of: 0,1. but it was a null.");
      }
    });

    it('works for integer enum', () => {
      enum Color { red = "redish", blue = "blueish" };
      const schema = tsEnum(Color);

      const result = schema.parse(null);

      if (result.success) {
        expect(false).toBe(true);
      } else {
        expect(result.error).toBe('Expected an enum member. It was supposed to be one of: "redish","blueish". but it was a null.');
      }
    });
  });



});