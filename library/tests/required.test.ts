import { number, object, optionalProperty, partial, required, string } from "../src";

const person = object(
  optionalProperty('name', string),
  optionalProperty('age', number)
);

describe('required tests', () => {
  const cases = [
    [1, required(person), { name: 'mike', age: 24 }, true],
    [2, required(person), { name: 'mike' }, false],
    [3, required(person), { age: 24 }, false],
    [4, required(person), {}, false],
    [5, required(person), null, false],
  ] as const;

  it.each(cases)('Case %i', (caseNumber, parser, data, shouldPass) => {
    expect(parser.parse(data).success).toEqual(shouldPass);
  });

  it('has a good error message', () => {
    const schema = partial(person).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      expect(result.error).toBe("Expected a Thing but it was a null.");
    } else {
      expect(false).toBe(true);
    }
  });
});