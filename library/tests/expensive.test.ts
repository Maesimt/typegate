import { Infer, array, discriminatedUnion, intersection, literal, number, object, optionalProperty, property, string, tsEnum, union } from "../src";

enum Sports {
    Volleyball,
    Basketball,
    Badminton,
    Tennis,
    Squash,
    Pingpong,
}

const hobby = union(
    literal("Boardgames"),
    literal("Painting"),
    tsEnum(Sports),
);

export type Hobby = Infer<typeof hobby>;

enum Job {
    Lawyer,
    Farmer,
    Developer,
    Manager,
    Boss,
}

enum KindOfSchool {
    Highschool,
    Bootcamp,
    College,
    University,
}

const baseSchool = object(
    property("id", string),
    property("name", string),
    property("operatingSince", string), // TODO: ISO-8601
    property("students", number),
    property("teachers", number),
)

const highSchool = intersection(
    baseSchool,
    object(
        property("kind", literal(KindOfSchool.Highschool)),
        property("availableHobbies", array(hobby)),
        property("officialSportTeams", array(tsEnum(Sports))),
    )
);

type Highschool = Infer<typeof highSchool>;

const college = intersection(
    baseSchool,
    object(
        property("kind", literal(KindOfSchool.College)),
        property("students", object(
            property("naturalScience", number),
            property("humanScience", number),
            property("technical", number),
        )),
    )
);

type College = Infer<typeof college>;


const school = discriminatedUnion("kind", highSchool, college);
type School = Infer<typeof school>;


const basePerson = object(
    property("id", string),
    property("name", string),
    property("age", number),
    optionalProperty("numberOfKids", number),
    optionalProperty("yearsOfExperence", number),
    optionalProperty("hobbies", array(hobby)),
);

const lawyer = intersection(
    basePerson,
    object(
        property("job", literal(Job.Lawyer)),
        property("wentTo", school),
        property("guiltyClients", number),
        optionalProperty("judgesCorrupted", number),
    )
);

type Lawyer = Infer<typeof lawyer>;

const person = discriminatedUnion("job", lawyer);
type Person = Infer<typeof person>;

it('Given an expansive to compute type the resolution is okay and the time is acceptable', () => {
    const result = person.parse(null);

    if (result.success) {
        result.value; // <--- Inspect resolution time here.
    }
    expect(true).toBe(true);
});