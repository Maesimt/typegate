import { nullable } from "../src";

describe('nullable tests', () => {
  const workingCases = [
    { label: "null", schema: nullable, value: null, shouldPass: true },
  ];

  const failingCases = [
    { label: "undefined", schema: nullable, value: undefined, shouldPass: false },
    { label: "123", schema: nullable, value: 123, shouldPass: false },
    { label: "'abc'", schema: nullable, value: 'abc', shouldPass: false },
    { label: "''", schema: nullable, value: '', shouldPass: false },
    { label: "[1,2,3]", schema: nullable, value: [1, 2, 3], shouldPass: false },
    { label: "[]", schema: nullable, value: [], shouldPass: false },
    { label: "{}", schema: nullable, value: {}, shouldPass: false },
    { label: "true", schema: nullable, value: true, shouldPass: false },
    { label: "false", schema: nullable, value: false, shouldPass: false },
  ];

  it.each([...workingCases, ...failingCases])('Expected the schema $label to be $shouldPass given this value "$value."  ', ({ schema, value, shouldPass }) => {
    expect(schema.parse(value).success).toEqual(shouldPass);
  });
});