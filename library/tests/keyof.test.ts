import { Equals, Infer, IsTrue, keyof } from "../src";

describe('boolean tests', () => {
  const Colors = {
    red: "red",
    blue: "blue",
    0: "grey",
  } as const;

  type Color = keyof typeof Colors;

  const parser = keyof(Colors);

  const workingCases = [
    { label: "red", parser, value: "red", shouldPass: true },
    { label: "blue", parser, value: "red", shouldPass: true },
    { label: "0", parser, value: 0, shouldPass: true },
  ];

  const failingCases = [
    { label: "null", parser, value: null, shouldPass: false },
    { label: "undefined", parser, value: undefined, shouldPass: false },
    { label: "123", parser, value: 123, shouldPass: false },
    { label: "'abc'", parser, value: 'abc', shouldPass: false },
    { label: "''", parser, value: '', shouldPass: false },
    { label: "[1,2,3]", parser, value: [1, 2, 3], shouldPass: false },
    { label: "[]", parser, value: [], shouldPass: false },
    { label: "{}", parser, value: {}, shouldPass: false },
  ];

  it.each([...workingCases, ...failingCases])('Expected the parser $label to be $shouldPass given this value "$value."  ', ({ parser, value, shouldPass }) => {
    expect(parser.parse(value).success).toEqual(shouldPass);
  });

  type InferredType = Infer<typeof parser>;
  type Check = IsTrue<Equals<Color, InferredType>>;
});