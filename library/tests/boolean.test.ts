import { boolean } from "../src";

describe('boolean tests', () => {
  const workingCases = [
    { label: "true", parser: boolean, value: true, shouldPass: true },
    { label: "false", parser: boolean, value: false, shouldPass: true },
  ];

  const failingCases = [
    { label: "null", parser: boolean, value: null, shouldPass: false },
    { label: "undefined", parser: boolean, value: undefined, shouldPass: false },
    { label: "123", parser: boolean, value: 123, shouldPass: false },
    { label: "'abc'", parser: boolean, value: 'abc', shouldPass: false },
    { label: "''", parser: boolean, value: '', shouldPass: false },
    { label: "[1,2,3]", parser: boolean, value: [1, 2, 3], shouldPass: false },
    { label: "[]", parser: boolean, value: [], shouldPass: false },
    { label: "{}", parser: boolean, value: {}, shouldPass: false },
  ];

  it.each([...workingCases, ...failingCases])('Expected the parser $label to be $shouldPass given this value "$value."  ', ({ parser, value, shouldPass }) => {
    expect(parser.parse(value).success).toEqual(shouldPass);
  });
});