import { intersection, number, object, optionalProperty, partial, property, string } from "../src";

const person = object(
  property('name', string),
  optionalProperty('age', number)
);

describe('intersection tests', () => {
  const cases = [
    [1, partial(person), { name: 'mike', age: 24 }, true],
    [2, partial(person), { name: 'mike' }, true],
    [3, partial(person), { age: 24 }, true],
    [4, partial(person), {}, true],
    [5, partial(person), null, false],
  ] as const;

  it.each(cases)('Case %i', (caseNumber, parser, data, shouldPass) => {
    expect(parser.parse(data).success).toEqual(shouldPass);
  });

  it('has a good error message', () => {
    const schema = partial(person).setTypeName("Thing");

    const result = schema.parse(null);

    if (!result.success) {
      expect(result.error).toBe("Expected a Thing but it was a null.");
    } else {
      expect(false).toBe(true);
    }
  });
});