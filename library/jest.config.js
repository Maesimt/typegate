/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: '.*\.test\.ts$',
  coverageReporters: ['text-summary', 'lcov', 'cobertura'],
  collectCoverageFrom: [
    "src/**/{!(ignore-me),}.ts"
  ]
};