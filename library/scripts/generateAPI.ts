import Parser from 'tree-sitter';
import plugin from 'tree-sitter-typescript';

import fs from 'node:fs';
import path from "node:path";


const dirToScan = path.join(__dirname, '../src');
console.log(`Directory to scan for source "${dirToScan}".`);

const filesToProcess = fs.readdirSync(dirToScan);

const parser = new Parser();
parser.setLanguage(plugin.typescript);

type JSDoc = {
    "@description": string,
    "@example": string,
    "@see": string,
}

type FunctionDetails = {
    name: string,
    JSDoc: JSDoc,
    file: string,
};

const functionDetails: FunctionDetails[] = [];

for (const file of filesToProcess) {
    console.log(`Extracting information from "${file}".`);
    const buffer = fs.readFileSync(path.join(dirToScan,"/", file));
    
    const sourceCode = buffer.toString();

    const tree = parser.parse(sourceCode);

    for(const child of tree.rootNode.children) {
        if (child.type !== 'comment') {
            continue;
        }
    
        const next = child.nextSibling;
        if (next === null || next.type !== 'export_statement') {
            continue;
        }
    
        const identifiers = next.descendantsOfType("identifier")
        if (identifiers.length === 0) {
            continue;
        }
        
        functionDetails.push({
            name: identifiers[0].text,
            JSDoc: parseComment(child.text),
            file: file,
        });
    }
}
console.log(`Extracted functions are: ${functionDetails.map(e => e.name).join(', ')}.`)
console.log(`Found ${functionDetails.length} functions.`)

function parseComment(comment: string) {
    const lines = comment.split("\n");
    lines.splice(0,1);
    lines.splice(lines.length - 1, 1);

    const nonEmptyLinesWithoutPrefix = [];
    for (const line of lines) {
        const l = line.replace(" * ", '');
        nonEmptyLinesWithoutPrefix.push(l);
    }

    let scope: "@description" | "@example" | "@see"  = "@description";
    const buckets = {
        "@description": [] as string[],
        "@example": [] as string[],
        "@see": [] as string[],
    };

    for(let line of nonEmptyLinesWithoutPrefix) {
        if (line.includes("@description")) {
            scope = "@description";
            line = line.replace("@description", "");
        }
        
        if (line.includes("@example")) {
            scope = "@example";
            line = line.replace("@example", "");
        }

        if (line.includes("@see")) {
            scope = "@see";
            line = line.replace("@see", "");
        }

        buckets[scope].push(line);
    }
    return {
        "@description": buckets['@description'].join("\n"),
        "@example": buckets['@example'].join("\n"),
        "@see": buckets['@see'].join("\n"),
    };
}


let apiFile = ""
for (const fn of functionDetails) {
    apiFile += `## ${fn.name}\n\n`;
    apiFile += `${fn.JSDoc['@description']}\n\n`;
    apiFile += "```ts twoslash\n";
    apiFile += `${fn.JSDoc['@example']}\n`;
    apiFile += "```\n\n";
}

// TODO: If not exist, create i
const pathForAPIDocumentation = path.join(__dirname, '../docs','/pages/api.mdx');
fs.writeFileSync(pathForAPIDocumentation, apiFile);
console.log(`Updated the documentation at "${pathForAPIDocumentation}".`)
