# TODOs

- DiscrimatedUnion should check that the key type is present in every schema (type-level only).

- Create some utility for the RecordKeySchemas type. Its looping over the keys quite often.

- More typescript utilities in priority order: Partial, Pick, Omit, Required, Record

- Intersection should only hold an object schema within it. And it should be built only once in the constructor.

- Optional Opt-in JQ command in the error message.

- Properly formatted error message. 

- Better error messages.

- Reduce code volume and redundancy to tackle bundle size.

- Better docs, example, jsdoc

- TBD: Automatic discriminated union optimization in the union constructor (Not sure if the behavior would be surprising sometimes.)

- Capture de stack trace up to entry in the library to not pollute userland code with our stacktrace. Parse= entrypoint stop, debug = fullstracktrace.

- Add a memory&cpu benchmark to an expensive parser + value. To help find memory optimization in the future.

