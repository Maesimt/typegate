import { Result } from "./result";

export type Options = {
  mode: "assert" | "createNewObjects"
}

export interface Schema<T> {
  parse: (value: unknown, options?: Options) => Result<string, T>;
  debug: (value: unknown, options?: Options) => Result<ParsingError, T>,
};

export const createSchema = <T>(fn: (value: unknown) => Result<ParsingError, T>): Schema<T> => ({
  parse: (value: unknown) => {
    return fn(value).mapError(toMessage);
  },
  debug: fn,
});

export type Infer<S> = S extends Schema<infer T> ? T : never;
export type Extract<T extends Schema<unknown>> = T['parse'];

// Remove the ParsingError suffix on everything in the union ?
export type ParsingError
  = BasicParsingError
  | RecordParsingError
  | ListParsingError
  | UnionParsingError
  | TupleSizeParsingError
  | TupleItemParsingError
  | CustomParsingError
  | EnumParsingError

export class RecordParsingError extends Error {
  readonly tag = 'RecordParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly receivedType: string;
  readonly key: string | number;
  readonly innerError?: ParsingError;

  constructor(args: {
    readonly parserName: string,
    readonly expectedType: string,
    readonly receivedValue: unknown,
    readonly key: string | number,
    readonly innerError?: ParsingError,
  }
  ) {
    super();
    this.parserName = args.parserName;
    this.expectedType = args.expectedType;
    this.receivedType = getType(args.receivedValue);
    this.key = args.key;
    this.innerError = args.innerError;
  }
}

export class ListParsingError extends Error {
  readonly tag = 'ListParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly receivedType: string;
  readonly index: number;
  readonly innerError: ParsingError;

  constructor(args: {
    readonly parserName: string,
    readonly expectedType: string,
    readonly receivedValue: unknown,
    readonly index: number,
    readonly innerError: ParsingError,
  }
  ) {
    super();
    this.parserName = args.parserName;
    this.expectedType = args.expectedType;
    this.receivedType = getType(args.receivedValue);
    this.index = args.index;
    this.innerError = args.innerError;
  }
}

export class TupleSizeParsingError extends Error {
  readonly tag = 'TupleSizeParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly expectedLength: number;
  readonly receivedLength: number;

  constructor(args: {
    readonly parserName: string,
    readonly expectedType: string,
    readonly expectedLength: number,
    readonly receivedLength: number,
  }
  ) {
    super();
    this.parserName = args.parserName;
    this.expectedType = args.expectedType;
    this.expectedLength = args.expectedLength;
    this.receivedLength = args.receivedLength;
  }
}

export class TupleItemParsingError extends Error {
  readonly tag = 'TupleItemParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly receivedType: string;
  readonly index: number;
  readonly innerError: ParsingError;

  constructor(args: {
    readonly parserName: string,
    readonly expectedType: string,
    readonly receivedValue: unknown,
    readonly index: number,
    readonly innerError: ParsingError,
  }
  ) {
    super();
    this.parserName = args.parserName;
    this.expectedType = args.expectedType;
    this.receivedType = getType(args.receivedValue);
    this.index = args.index;
    this.innerError = args.innerError;
  }
}

export class UnionParsingError extends Error {
  readonly tag = 'UnionParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly wasNotOneOfThose: string[];
  readonly receivedType: string;

  constructor(args: {
    readonly parserName: string,
    readonly expectedType: string,
    readonly wasNotOneOfThose: string[];
    readonly receivedValue: unknown,
  }) {
    super();
    this.parserName = args.parserName;
    this.expectedType = args.expectedType;
    this.wasNotOneOfThose = args.wasNotOneOfThose;
    this.receivedType = getType(args.receivedValue);
  }
}

export class EnumParsingError extends Error {
  readonly tag = 'EnumParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly wasNotOneOfThose: string[];
  readonly receivedType: string;

  constructor(args: {
    readonly parserName: string,
    readonly expectedType: string,
    readonly wasNotOneOfThose: string[];
    readonly receivedValue: unknown,
  }) {
    super();
    this.parserName = args.parserName;
    this.expectedType = args.expectedType;
    this.wasNotOneOfThose = args.wasNotOneOfThose;
    this.receivedType = getType(args.receivedValue);
  }
}

export class BasicParsingError extends Error {
  readonly tag = 'BasicParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly receivedType: string;
  readonly innerError?: ParsingError;

  private constructor(
    parserName: string,
    expectedType: string,
    receivedType: string,
    innerError?: ParsingError,
  ) {
    super();
    this.parserName = parserName;
    this.expectedType = expectedType;
    this.receivedType = receivedType;
    this.innerError = innerError;
  }

  static from(args: {
    parserName: string,
    expectedType: string,
    receivedValue: unknown,
    innerError?: ParsingError
  }) {
    return new BasicParsingError(args.parserName, args.expectedType, getType(args.receivedValue), args.innerError);
  }
}

export class CustomParsingError extends Error {
  // This class is for general usage. The other one are for internal use for primitive parser (rec,list,arr)
  // If it was a lib, then it would be the only export ParsingError constructor.
  readonly tag = 'CustomParsingError';
  readonly parserName: string;
  expectedType: string;
  readonly receivedType: string;
  readonly innerError?: ParsingError;

  private constructor(
    parserName: string,
    expectedType: string,
    receivedType: string,
    innerError?: ParsingError,
  ) {
    super();
    this.parserName = parserName;
    this.expectedType = expectedType;
    this.receivedType = receivedType;
    this.innerError = innerError;
  }

  static from(
    expectedType: string,
    receivedValue: unknown,
    innerError: ParsingError | undefined = undefined
  ) {
    return new CustomParsingError(expectedType, expectedType, getType(receivedValue), innerError);
  }
}

const addArticle = (word: string, capitalize: boolean = false) => {
  if (word.length > 0 && ['a', 'e', 'i', 'o', 'u', 'y'].includes(word[0])) {
    return `${capitalize ? 'A' : 'a'}n ${word}`;
  }
  return `${capitalize ? 'A' : 'a'} ${word}`;
}


type PathStack = (string | number)[];

function toJSONPath(pathStack: PathStack) {
  let jsonPath = '';

  for (let i = 0; i < pathStack.length; i++) {
    const frame = pathStack[i];
    if (typeof frame === 'number') {
      jsonPath += `[${frame}]`;
    }

    if (typeof frame === 'string') {
      jsonPath += frame;
    }

    if (i < pathStack.length - 1) {
      if (typeof pathStack[i + 1] === 'string') {
        jsonPath += '.'
      }
    }
  }
  return jsonPath;
}

type ErrorContext = {
  error: ParsingError;
  firstExpectedType: string;
  pathStack: PathStack;
}

export function toMessage(error: ParsingError): string {
  return messageExtraction({ error, firstExpectedType: error.expectedType, pathStack: [] })
}

export function messageExtraction(context: ErrorContext): string {
  let currentContext = { ...context };

  while (true) {
    const { error, pathStack, firstExpectedType } = currentContext;

    switch (error.tag) {
      case "ListParsingError": {
        currentContext = {
          ...currentContext,
          error: error.innerError,
          pathStack: pathStack.concat([error.index]),
        };
        continue;
      }

      case "RecordParsingError": {
        // Required key is missing (It's a leaf you can't have anything after)
        if (error.innerError) {
          currentContext = {
            ...currentContext,
            error: error.innerError,
            pathStack: pathStack.concat([error.key]),
          };
          continue;
        }

        const missingKeyMessage = `The ${error.expectedType} is missing the required property "${error.key}"`;
        if (pathStack.length > 0) {
          return missingKeyMessage + ` in "<root>.${toJSONPath(pathStack)}".`;
        } else {
          return missingKeyMessage + ".";
        }
      }

      case "UnionParsingError": {
        let msg = `Failed to parse ${addArticle(firstExpectedType)}. We received ${addArticle(error.receivedType)}`;
        const path = toJSONPath(pathStack);
        if (path.length > 0) {
          msg += ` at "<root>.${path}"`;
        } else {
          msg += "";
        }
        return `${msg} but it was supposed to one of (${error.wasNotOneOfThose.join(' | ')}).`;
      }

      case "EnumParsingError": {
        let msg = `Expected ${addArticle(firstExpectedType)}`;
        const path = toJSONPath(pathStack);
        if (path.length > 0) {
          msg += `at "<root>.${path}"`;
        } else {
          msg += ".";
        }
        return `${msg} It was supposed to be one of: ${error.wasNotOneOfThose.join(',')}. but it was ${addArticle(error.receivedType)}.`;
      }

      case "TupleSizeParsingError": {
        return `Expected ${addArticle(firstExpectedType)} and the length was supposed to be ${error.expectedLength} but it was ${error.receivedLength}.`;
      }

      case "TupleItemParsingError": {
        if (error.innerError) {
          currentContext = {
            ...currentContext,
            error: error.innerError,
            pathStack: pathStack.concat([error.index]),
          };
          continue;
        }
      }

      case "CustomParsingError":
      case "BasicParsingError": {
        if (error.innerError) {
          currentContext = {
            ...currentContext,
            error: error.innerError,
          };
          continue;
        }
        // If first pathstack === 1 dont add the prefix with firstexpectedtype

        if (pathStack.length > 0) {

          return `Expected ${addArticle(firstExpectedType)} and the value at "${typeof pathStack[0] === 'string' ? "<root>." : ""}${toJSONPath(pathStack)}" was supposed to be ${addArticle(error.expectedType)} but it was ${addArticle(error.receivedType)}.`;
        } else {
          return `Expected ${addArticle(firstExpectedType)} but it was ${addArticle(error.receivedType)}.`;
        }
      }
    }
  }
}

const getType = (value: unknown): string => {
  if (value === null) {
    return 'null';
  }

  if (value === undefined) {
    return 'undefined'
  }
  if (Array.isArray(value)) {
    return 'array';
  }

  if (typeof value === 'object') {
    if (value.constructor && value.constructor.prototype) {
      const properties = Object.getOwnPropertyNames(value.constructor.prototype);
      const classname = value.constructor.name;
      if (properties.length > 0 && properties[0] === 'constructor' && classname !== 'Object') {
        return `${classname} class instance`;
      }
    }

    return 'object';
  }

  return typeof value;
}

