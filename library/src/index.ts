import { array, tuple } from './array';
import { object, required,  intersection, partial, property, optionalProperty, pick, omit } from './object';
import { Result, success, failure, map } from './result';
import { ParsingError, CustomParsingError, Infer, createSchema } from './parser';
import { discriminatedUnion, union } from './union';
import { lazy } from './lazy';
import { noCheck } from './noCheck';
import { tsEnum } from './enum';
import { record } from './record';
import { literal, undefined, string, number, nullable, any, unknown, boolean, keyof } from './primitives';
import { Equals, IsTrue } from './equals';

const t: {
  undefined: typeof undefined,
  null: typeof nullable,
  any: typeof any,
  unknown: typeof unknown,
  literal: typeof literal,
  boolean: typeof boolean,
  number: typeof number,
  string: typeof string,
  enum: typeof tsEnum,
  array: typeof array,
  tuple: typeof tuple,
  object: typeof object,
  property: typeof property,
  optionalProperty: typeof optionalProperty,
  union: typeof union,
  discriminatedUnion: typeof discriminatedUnion,
  intersection: typeof intersection,
  required: typeof required,
  partial: typeof partial,
  omit: typeof omit,
  pick: typeof pick,
  noCheck: typeof noCheck,
  lazy: typeof lazy,
  record: typeof record,
  keyof: typeof keyof
} = {
  undefined,
  null: nullable,
  any,
  unknown,
  literal,
  boolean,
  number,
  string,
  enum: tsEnum,
  array,
  tuple,
  object,
  property,
  optionalProperty,
  union,
  discriminatedUnion,
  intersection,
  required,
  partial,
  omit,
  pick,
  noCheck,
  lazy,
  record,
  keyof,
};

export {
  t,
  lazy,
  tsEnum,
  createSchema,
  noCheck,
  any,
  unknown,
  record,
  array,
  object,
  property,
  optionalProperty,
  intersection,
  required,
  partial,
  pick,
  omit,
  tuple,
  union,
  discriminatedUnion,
  literal,
  undefined,
  string,
  number,
  nullable,
  Result,
  success,
  failure,
  map,
  boolean,
  CustomParsingError,
  ParsingError,
  Infer,
  Equals,
  IsTrue,
  keyof
};