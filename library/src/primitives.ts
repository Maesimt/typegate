import { BasicParsingError, ParsingError, Schema, toMessage } from "./parser";
import { Result, failure, success } from "./result";

type LiteralCompatible
  = string
  | number
  | boolean;

class LiteralSchema<T extends LiteralCompatible> implements Schema<T> {
  literal: T;
  constructor(literal: T) {
    this.literal = literal;
  }

  parse(value: unknown){
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    if (value === this.literal) {
      return success(this.literal);
    }

    return failure(BasicParsingError.from({
      expectedType: JSON.stringify(this.literal),
      parserName: "literal",
      receivedValue: value,
    }))
  }
}

/**
 * @description
 * You can use literal when you know the value is supposed to be a specific one.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.literal(1);
 * type Schema = Infer<typeof schema>;
 * //     🔎 1
 * 
 * schema.parse(1);    // ✅ Success
 * schema.parse(true); // ❌ Failure
 * 
 * enum Color { red, blue };
 * 
 * const schema2 = t.literal(Color.blue);
 * 
 * schema2.parse(Color.blue); // ✅ Success
 * schema2.parse(Color.red);  // ❌ Failure
 * 
 * type Schema2 = Infer<typeof schema2>;
 * //     🔎 Color.blue
 * @see {@link https://typegate.dev/#/building-blocks?id=literal docs}
 */
export const literal = <T extends LiteralCompatible>(literal: T) => new LiteralSchema(literal);

class UndefinedSchema implements Schema<undefined> {
  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    if (typeof value === 'undefined') {
      return success(value);
    }

    return failure(BasicParsingError.from({
      expectedType: 'undefined',
      parserName: "undefind",
      receivedValue: value,
    }));
  }
}
/**
 * A schema that passes for "undefined" and nothing else.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.undefined;
 * //      🔎 undefined
 * 
 * schema.parse(undefined); // ✅ Success
 * schema.parse(null);      // ❌ Failure
 * schema.parse(false);     // ❌ Failure
 * @see {@link https://typegate.dev/#/building-blocks?id=undefined docs}
 */
export const undefined = new UndefinedSchema();

class NullSchema implements Schema<null> {
  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    if (value === null) {
      return success(value);
    }

    return failure(BasicParsingError.from({
      expectedType: 'null',
      parserName: "nullable",
      receivedValue: value,
    }))
  }
}

/**
 * A schema that passes for "null" and nothing else.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.null;
 * //      🔎 null
 * 
 * schema.parse(null);      // ✅ Success
 * schema.parse(undefined); // ❌ Failure
 * schema.parse(false);     // ❌ Failure
 * @see {@link https://typegate.dev/#/building-blocks?id=nullable docs}
 */
export const nullable = new NullSchema();

class NumberSchema implements Schema<number> {
  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    if (typeof value === 'number') {
      return success(value);
    }

    return failure(BasicParsingError.from({
      expectedType: 'number',
      parserName: "number",
      receivedValue: value,
    }))
  }
}

/**
 * A schema that passes for "number" and nothing else.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.number;
 * //      🔎 number
 * 
 * schema.parse(123);   // ✅ Success
 * schema.parse(1.01);  // ✅ Success
 * schema.parse(1_000); // ✅ Success
 * schema.parse("123"); // ❌ Failure
 * schema.parse({});    // ❌ Failure
 * @see {@link https://typegate.dev/#/building-blocks?id=number docs}
 */
export const number = new NumberSchema();


export class StringSchema implements Schema<string> {
  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    if (typeof value === 'string') {
      return success(value);
    }

    return failure(BasicParsingError.from({
      expectedType: 'string',
      parserName: "string",
      receivedValue: value,
    }));
  }
}

/**
 * A schema that passes for "string" and nothing else.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.string;
 * //      🔎 string
 * 
 * schema.parse("abc");     // ✅ Success
 * schema.parse("");        // ✅ Success
 * schema.parse(123);       // ❌ Failure
 * schema.parse(undefined); // ❌ Failure
 * schema.parse([]);        // ❌ Failure
 * @see {@link https://typegate.dev/#/building-blocks?id=string docs}
 */
export const string = new StringSchema();

export class BooleanSchema implements Schema<boolean> {
  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    if (typeof value === 'boolean') {
      return success(value);
    }

    return failure(BasicParsingError.from({
      expectedType: 'boolean',
      parserName: "boolean",
      receivedValue: value,
    }));
  };
}

/**
 * A schema that passes for "boolean" and nothing else.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.boolean;
 * //      🔎 boolean
 * 
 * schema.parse(true);     // ✅ Success
 * schema.parse(false);    // ✅ Success
 * schema.parse(1);        // ❌ Failure
 * @see {@link https://typegate.dev/#/building-blocks?id=boolean docs}
 */
export const boolean = new BooleanSchema();

class UnknownSchema implements Schema<unknown> {
  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    return success(value);
  };
}
/**
 * A schema that passes for everything and sets the type to "unknown".
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.unknown;
 * //      🔎 unknown
 * 
 * schema.parse(true);  // ✅ Success
 * schema.parse(false); // ✅ Success
 * schema.parse(1);     // ✅ Success
 * schema.parse(null);  // ✅ Success
 * @see {@link https://typegate.dev/#/building-blocks?id=unknown docs}
 */
export const unknown = new UnknownSchema();

class AnySchema implements Schema<any> {
  parse(value: unknown): Result<string, any> {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown): Result<ParsingError, any> {
    return success(value);
  };
}
/**
 * A schema that passes for everything and sets the type to "any".
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.any;
 * //      🔎 any
 * 
 * schema.parse(true);  // ✅ Success
 * schema.parse(false); // ✅ Success
 * schema.parse(1);     // ✅ Success
 * schema.parse(null);  // ✅ Success
 * @see {@link https://typegate.dev/#/building-blocks?id=any docs}
 */
export const any = new AnySchema();

class KeyOfSchema<K extends string | number | symbol> implements Schema<K> {
  keys: K[];
  constructor(keys: K[]) {
    this.keys = keys;
  }

  static from<T extends Record<string | number | symbol, unknown>>(object: T): KeyOfSchema<keyof T> {
    const keys = Object.keys(object);
    return new KeyOfSchema(keys);
  }

  parse(value: unknown): Result<string, K>{
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown): Result<ParsingError, K> {
    if (this.keys.some(key => {
      if (typeof value === 'number') {
        return value.toString() === key;
      }
      return value === key;
    })) {
      return success(value as K);
    }

    return failure(BasicParsingError.from({
      expectedType: "keyof",
      parserName: "keyof",
      receivedValue: value,
    }))
  }
}
/**
 * Gets the keys of a record.
 * 
 * @example
 * // TBD
 * @see {@link https://typegate.dev/#/building-blocks?id=any docs}
 */
export const keyof = <T extends Record<string | number | symbol, unknown>>(object: T) => KeyOfSchema.from(object);