import { BasicParsingError, Schema,  ListParsingError,  TupleItemParsingError, TupleSizeParsingError, toMessage, Options, Infer, ParsingError } from "./parser";
import { Result, failure, success } from "./result";

class ArraySchema<T> implements Schema<T[]> {
  schema: Schema<T>;

  constructor(schema: Schema<T>) {
    this.schema = schema;
  }

  parse(value: unknown, options?: Options) {
    return this.debug(value, options).mapError(toMessage);
  }

  debug(value: unknown, options?: Options) {
    if (!Array.isArray(value)) {
      return failure(BasicParsingError.from({
        expectedType: "array",
        receivedValue: value,
        parserName: "array",
      }));
    }

    const list: T[] = [];

    for (let i = 0; i < value.length; i++) {
      const result = this.schema.debug(value[i], options);

      if (!result.success) {
        return failure(new ListParsingError({
          expectedType: "array",
          receivedValue: value,
          parserName: "array",
          index: i,
          innerError: result.error,
        }));
      }

      list.push(result.value as T);
    }

    return success(list);
  }

}

/**
 * The value must be an array and all items must pass the given schema.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.array(t.string);
 * type Schema = Infer<typeof schema>;
 * //     🔎 string[]
 * 
 * schema.parse(["abc","def"]);   // ✅ Success 
 * schema.parse(["abc", 12]);     // ❌ Failure
 * schema.parse([]);              // ✅ Success
 * schema.parse([{}, {}, {}]);    // ❌ Failure
 * schema.parse({name: "bob"});   // ❌ Failure
 * @see {@link https://typegate.dev Need help? Official Guide}
 */
export const array = <T>(schema: Schema<T>) => new ArraySchema<T>(schema);


export class TupleSchema<S extends [...Schema<unknown>[]]> implements Schema<S extends readonly unknown[] ? { [Index in keyof S]: Infer<S[Index]> } : never> {
  _schemas: S;

  constructor(...args: S) {
    this._schemas = args;
  }

  parse(value: unknown, options?: Options) {
    return this.debug(value, options).mapError(toMessage);
  }

  debug(values: unknown, options?: Options): Result<ParsingError, S extends readonly unknown[] ? { [Index in keyof S]: Infer<S[Index]> } : never> {
    if (!Array.isArray(values)) {
      return failure(BasicParsingError.from({
        parserName: "tuple",
        expectedType: "tuple",
        receivedValue: values,
      }));
    }

    if (this._schemas.length !== values.length) {
      return failure(new TupleSizeParsingError({
        parserName: "tuple",
        expectedType: "tuple",
        expectedLength: this._schemas.length,
        receivedLength: values.length,
      }))
    }


    const tuple = [];
    for (let i = 0; i < this._schemas.length; i++) {
      const result = this._schemas[i].debug(values[i], options)

      if (!result.success) {
        return failure(new TupleItemParsingError({
          parserName: "tuple",
          expectedType: "tuple",
          receivedValue: values,
          index: i,
          innerError: result.error,
        }));
      }

      tuple.push((result.value))
    }

    return success(tuple as any);
  }
}

/**
 * The value must be an array of N items.
 * Each item must pass the schema corresponding
 * to its position.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.tuple(t.string, t.number);
 * type Schema = Infer<typeof schema>;
 * //     🔎 [string, number]
 * 
 * schema.parse(["abc", 123]);   // ✅ Success
 * schema.parse(["abc", "def"]); // ❌ Failure
 * schema.parse(["abc"])         // ❌ Failure
 * schema.parse([])              // ❌ Failure
 * schema.parse(null);           // ❌ Failure
 * @see {@link https://typegate.dev Need help? Official Guide}
 */
export const tuple = <S extends [...Schema<unknown>[]]>(...args: S) => new TupleSchema(...args);



