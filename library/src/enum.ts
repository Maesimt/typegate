import { EnumParsingError, Schema, toMessage } from "./parser";
import { failure, success } from "./result";

class EnumSchema<TEnumKey extends string, TEnumValue extends string | number, Ref extends { [key in TEnumKey]: TEnumValue }> implements Schema<Ref[keyof Ref]> {
  ref: Ref;

  constructor(enumObject: Ref) {
    this.ref = enumObject;
  }

  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    let enumValues = Object.values(this.ref)
    if (enumValues.some(e => typeof e === 'number')) {
      enumValues = enumValues.filter(e => typeof e === 'number');
    }

    if (enumValues.includes(value)) {
      return success(value as Ref[keyof Ref]);
    };

    // TODO: Custom error with the values 
    // Enum name console.log(Object.keys({Color})[0]);
    // Values expected
    return failure(new EnumParsingError({
      expectedType: "enum member",
      parserName: "tsEnum",
      receivedValue: value,
      wasNotOneOfThose: enumValues.map(e => JSON.stringify(e)),
    }));
  }
}

/**
 * The value must be a member of 
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * enum NumberBasedColor { red, blue };
 * const schema = t.enum(NumberBasedColor);
 * type Schema = Infer<typeof schema>;
 * //     🔎 NumberBasedColor
 * 
 * schema.parse(NumberBasedColor.red); // ✅ Succcess
 * schema.parse(0);                    // ✅ Success
 * schema.parse("red");                // ❌ Failure
 * schema.parse(null);                 // ❌ Failure
 * 
 * enum StringBasedColor { red = "red", blue = "blue"};
 * const schema2 = t.enum(StringBasedColor);
 * type Schema2 = Infer<typeof schema2>;
 * //     🔎 StringBasedColor
 * 
 * schema2.parse(StringBasedColor.red); // ✅ Succcess
 * schema2.parse("red");                // ✅ Succcess
 * schema2.parse(0);                    // ❌ Failure
 * schema2.parse(null);                 // ❌ Failure
 * @see {@link https://typegate.dev Need help? Official Guide}
 */
export const tsEnum = <TEnumKey extends string, TEnumValue extends string | number, Ref extends { [key in TEnumKey]: TEnumValue }>(ref: Ref) => new EnumSchema(ref);

