import { object, ObjectSchema, property } from "./object";
import { ParsingError, Schema, toMessage, UnionParsingError } from "./parser";
import { boolean, number, string } from "./primitives";
import { failure, Result, success } from "./result";


type ArrayToUnion<S, output = never> = S extends [infer head, ...infer tail]
  ?
  (head extends Schema<infer headtype>
    ? ArrayToUnion<tail, output | headtype>
    : ArrayToUnion<tail, output>
  )
  : output;

  class UnionSchema<S extends Schema<unknown>[]> implements Schema<ArrayToUnion<S>> {
  schemas: S;
  name: string | null = null;

  constructor(...arr: S) {
    this.schemas = arr;
  }

  setTypeName(name: string) {
    this.name = name;
    return this;
  }

  parse(value: unknown): Result<string, ArrayToUnion<S>>{
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown): Result<ParsingError, ArrayToUnion<S>> {
    const typesItWasNot: string[] = [];

    for (const parser of this.schemas) {
      const result = parser.debug(value);

      if (result.success) {
        return success(result.value as any);
      } else {
        typesItWasNot.push(result.error.expectedType);
      }
    }

    return failure(new UnionParsingError({
      expectedType: this.name || "member of the union",
      wasNotOneOfThose: typesItWasNot,
      receivedValue: value,
      parserName: "union",
    }));
  };
}

/**
 * Schema to validate that the data is a member of an union.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.union(t.string, t.number);
 * //      🔎 string | number
 * 
 * schema.parse("abc"); // ✅ Success
 * schema.parse(123);   // ❌ Failure
 * schema.parse(123);   // ✅ Success
 * @see {@link https://typegate.dev/#/building-blocks?id=union docs}
 */
export const union = <S extends Schema<unknown>[]>(...arr: S) => new UnionSchema(...arr);

class DiscriminatedUnionSchema<K extends string, S extends ObjectSchema<unknown>[]> implements Schema<ArrayToUnion<S>> {
  _discriminatedKey: K;
  _schemas: S;
  name: string | null = null;

  constructor(discriminatedKey: K, ...arr: S) {
    this._discriminatedKey = discriminatedKey;
    this._schemas = arr;
  }

  setTypeName(name: string) {
    this.name = name;
    return this;
  }

  parse(value: unknown): Result<string, ArrayToUnion<S>> {
    return this.debug(value).mapError(toMessage); 
  }

  debug(value: unknown): Result<ParsingError, ArrayToUnion<S>> {
    // TODO: The key schema can be computed/stored at creation.
    const keySchema = object(
      property(this._discriminatedKey, union(string, number, boolean))
    ).setTypeName(this.name + "discriminatedKey");

    const resultA = keySchema.debug(value);

    if (!resultA.success) {
      resultA.error.expectedType = this.name || 'object';
      return failure(resultA.error);
    }


    // We need to find the first schema that has the discriminated key.
    // TODO: This can be known at creation instead of parsing time. (Key parsing schema by schema)
    const firstSchema = this._schemas.find((s) => {
      let schemas = [s]; // Could be a single object, or and intersection which has two parts.

      while (true) {
        if (schemas.length === 0) {
          break;
        }
        let schema = schemas[schemas.length - 1];

        if (schema instanceof ObjectSchema) {
          // Don't use concat, change it in place.
          const keySchema = schema.properties.find(p => p.key === this._discriminatedKey);

          if (keySchema) {
            return keySchema.schema.debug((value as any)[this._discriminatedKey]).success;
          }
          // Could add the failed parser to the type it was not in case of an error?
          // Maybe not worth optimizing for the worst case.
        }
        schemas.pop();
      }
      return false;
    });

    if (!firstSchema) {
      const typesItWasNot = [];
      for (const schema of this._schemas) {
        typesItWasNot.push(schema.name || 'object');
      }

      return failure(new UnionParsingError({
        expectedType: this.name || "member of the union",
        wasNotOneOfThose: typesItWasNot,
        receivedValue: value,
        parserName: "union",
      }));
    }

    const result = firstSchema.debug(value);

    if (result.success) {
      return success(result.value as ArrayToUnion<S>);
    } else {
      return failure(result.error);
    }
  }
}

/**
 * Same as union but only check the member schema that makes sense for the input.
 * 
 * @example
 * // TODO
 * @see {@link https://typegate.dev/#/building-blocks?id=discriminated-union docs}
 */
export const discriminatedUnion = <K extends string, T extends ObjectSchema<unknown>[]>(discriminatedKey: K, ...arr: T) => new DiscriminatedUnionSchema(discriminatedKey, ...arr);
