import { TupleSchema, array, tuple } from "./array";
import { BasicParsingError, Infer, Options, ParsingError, RecordParsingError, Schema, toMessage } from "./parser";
import { StringSchema, literal, number, string } from "./primitives";
import { Result, failure, success } from "./result";
import { union } from "./union";

export const record = <T extends string, K extends Schema<T>, V extends Schema<unknown>>(key: K, value: V) => new RecordSchema<T,K,V>(key,value);

export class RecordSchema<T extends string, K extends Schema<T>, V extends Schema<unknown>> implements Schema<Record<Infer<K>, Infer<V>>> {
  key: K;
  value: V;
  name: string | null = null;

  constructor(key: K, value: V) {
    this.key = key;
    this.value = value;
  }

  setTypeName(name: string) {
    this.name = name;
    return this;
  }
  
  parse(value: unknown, options?: Options): Result<string, Record<Infer<K>,Infer<V>>> {
    return this.debug(value, options).mapError(toMessage);
  }

  debug(value: unknown, options?: Options): Result<ParsingError, Record<Infer<K>,Infer<V>>> {
    if (typeof value !== 'object' || Array.isArray(value) || !value) {
        return failure(BasicParsingError.from({
            expectedType: this.name || "record",
            receivedValue: value,
            parserName: "record",
        }));
    }
    
    const keys = Object.keys(value);
    // Theory, I think its less expensive to check all the keys before starting to check their values.
    for (const key of keys) {
        const result = this.key.debug(key);
        if (!result.success) {
            // Maybe a wrapped error with a nicer message to.
            return failure(new RecordParsingError({
                expectedType: this.name || "record",
                receivedValue: value,
                parserName: "record",
                key: key,
                innerError: result.error,
              }));
        }
    }

    for (const key of keys) {
        const result = this.value.debug((value as any)[key]);
        if (!result.success) {
            // Maybe a wrapped error with a nicer message to.
            return failure(new RecordParsingError({
                expectedType: this.name || "record",
                receivedValue: value,
                parserName: "record",
                key: key,
                innerError: result.error,
              }));
        }
    }

    return success(value as any);
  }
}