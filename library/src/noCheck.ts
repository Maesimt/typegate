import { ParsingError, Schema, toMessage } from "./parser";
import { Result, success } from "./result";

class NoCheckSchema<T> implements Schema<T> {
  parse(value: unknown): Result<string, T> {
    return this.debug(value).mapError(toMessage);
  }
  
  debug(data: unknown): Result<ParsingError, T> {
    return success(data as any);
  }
}

/**
 * noCheck accepts a Type and will return a parser
 * that always succeeds to parse.
 * 
 * It is not validating anything.
 * 
 * It's useful when you are migrating big types
 * and one of the type does not have a parser yet.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const schema = t.noCheck<number>();
 * type Schema = Infer<typeof schema>;
 * //     🔎 number
 * 
 * schema.parse(123);            // ✅ Success
 * schema.parse(null);           // ✅ Success  
 * schema.parse(["abc", "def"]); // ✅ Success
 * 
 * // ----------------------------------------------
 * 
 * type Person = { name: string, age: number};
 * const personSchema = t.noCheck<Person>();
 * type InferredPerson = Infer<typeof personSchema>;
 * //       🔎 Person
 * 
 * personSchema.parse(123);            // ✅ Success
 * personSchema.parse(null);           // ✅ Success  
 * personSchema.parse(["abc", "def"]); // ✅ Success
 * personSchema.parse(["abc", "def"]); // ✅ Success
 * @see {@link https://typegate.dev Need help? Official Guide}
 */
export const noCheck = <T>() => new NoCheckSchema<T>();