import { Schema, toMessage } from "./parser";

class LazySchema<T> implements Schema<T> {
  _parser: () => Schema<T>;

  constructor(parser: () => Schema<T>) {
    this._parser = parser;
  }

  parse(value: unknown) {
    return this.debug(value).mapError(toMessage);
  }

  debug(value: unknown) {
    return this._parser().debug(value);
  };
}

/**
 * Useful when you want to define a schema that calls itself.
 * 
 * The type annonation is mandatory and inference is unavailable unfortunately.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * type Person = {
 *   name: string;
 *   father?: Person; // This type is recursive.
 * };
 * 
 * const schema = t.object(
 *   t.property('name', t.string),
 *   t.optionalProperty('father', t.lazy<Person>(() => schema)),
 * );
 * 
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name: string, father?: Person | undefined }
 * @see {@link https://typegate.dev Need help? Official Guide}
 */
export const lazy = <T>(lazySchema: any) => new LazySchema<T>(lazySchema as any);