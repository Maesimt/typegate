import { BasicParsingError, Schema, RecordParsingError, toMessage, Options } from "./parser";
import { failure, success } from "./result";

type ArrayToIntersection<S, output extends Record<string | number, unknown> = {}> = S extends [infer head, ...infer tail]
  ?
  (head extends Schema<infer headtype>
    ?
    (headtype extends Record<string | number, unknown>
      ? ArrayToIntersection<tail, output & headtype>
      : never
    )
    : ArrayToIntersection<tail, output>
  )
  : output;

/**
 * Merge two object schemas together.
 * 
 * @example
 * import { t, Infer } from "typegate"; 
 * 
 * const withName = t.object(t.property('name', t.string));
 * const withAge = t.object(t.property('age', t.number));
 * 
 * const schema = t.intersection(withName, withAge);
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name: string, age: number }
 * 
 * schema.parse({ name: 'mike', age: 24 }); // ✅ Success
 * schema.parse({ name: 'mike' });          // ❌ Failure
 * schema.parse({ age: 24 });               // ❌ Failure
 * schema.parse({});                        // ❌ Failure
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const intersection = <O extends ObjectSchema<unknown>[]>(...args: O): ObjectSchema<ArrayToIntersection<O>> => {
  const properties = [];

  for (const objectSchema of args) {
    for (const property of objectSchema.properties) {
      properties.push(property);
    }
  }
  return new ObjectSchema(properties) as any;
}

type ObjectProperty<T>
  = Property<string, T>
  | OptionalProperty<string, T>;

type ObjectFromProperties<F extends ObjectProperty<unknown>[]> =
  F extends [infer Head, ...infer Tail]
  ? Tail extends ObjectProperty<unknown>[]
  ? (
    Head extends Property<infer keyType, infer schemaType>
    ? { [key in keyType]: schemaType } & ObjectFromProperties<Tail>
    : Head extends OptionalProperty<infer keyType, infer schemaType>
    ? { [key in keyType]?: schemaType } & ObjectFromProperties<Tail>
    : never
  )
  : never
  : {};

// TODO: Problem with intersect {bob: union(num,str)} & {bob: nonEmptyString} == {bob: union(str, nonEmptyString)}
// flattening of 
// export function object<T extends Schema<Record<string, unknown>>[]>(...parsers: T): Schema<Intersection<T>> {

/**
 * Define an object that has some properties and optional properties.
 * 
 * @example
 * import { t, Infer } from "typegate"; 
 * 
 * const schema = t.object(
 *     t.property('name', t.string),
 *     t.property('age', t.number),
 *     t.optionalProperty('isSmoker', t.boolean),
 * );
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name: string, age: number, isSmoker?: boolean }
 * 
 * schema.parse({ name: 'mike', age: 24, isSmoker: true }); // ✅ Success
 * schema.parse({ name: 'mike', age: 24 });                 // ✅ Success
 * schema.parse({ age: 24 });                               // ❌ Failure
 * schema.parse({});                                        // ❌ Failure
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const object = <T extends ObjectProperty<unknown>[]>(...args: T) => ObjectSchema.from(...args);

export class ObjectSchema<T> implements Schema<T> {
  properties: ObjectProperty<unknown>[];
  name: string | null = null;

  constructor(properties: ObjectProperty<unknown>[]) {
    this.properties = properties;
  }

  static from<T extends ObjectProperty<unknown>[]>(...properties: T): ObjectSchema<ObjectFromProperties<T>> {
    return new ObjectSchema(properties);
  }

  setTypeName(name: string) {
    this.name = name;
    return this;
  }

  parse(value: unknown, options?: Options) {
    return this.debug(value, options).mapError(toMessage);
  }

  debug (data: unknown, options?: Options) {
    if (typeof data !== 'object' || !data) return failure(BasicParsingError.from({
      expectedType: this.name || "object",
      receivedValue: data,
      parserName: "object",
    }));

    // TODO: if we want strict, we could define output and push to it, instead of just validating the data.
    let output = {} as T;

    // Required properties.
    for (const p of this.properties) {
      if (p.key in data) {
        const valueOfKey = (data as any)[p.key];
        const result = p.schema.debug(valueOfKey, options);
        if (!result.success) {
          return failure(new RecordParsingError({
            expectedType: this.name || "object",
            receivedValue: data,
            parserName: "required",
            key: p.key,
            innerError: result.error,
          }));
        }
        if (options?.mode === 'createNewObjects') {
          (output as any)[p.key] = result.value;
        }
      } else if (p instanceof Property) {
        return failure(new RecordParsingError({
          expectedType: this.name || "object",
          receivedValue: data,
          parserName: "optional",
          key: p.key,
        }));
      } 
      
    }

    if (options?.mode === 'createNewObjects') {
      return success(output);
    }
    return success(data as T);
  }
}

/**
 * Define a required property on an object. Meaning the key is always present 
 * and the value must match the schema.
 * 
 * 
 * @example
 * import { t, Infer } from "typegate"; 
 * 
 * const schema = t.object(
 *     t.property('name', t.string),
 * );
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name: string }
 * 
 * schema.parse({ name: 'mike' }); // ✅ Success
 * schema.parse({ name: 123 });    // ❌ Failure
 * schema.parse({});               // ❌ Failure 
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const property = <K extends string, T>(key: K, schema: Schema<T>) => new Property(key, schema);

class Property<K extends string, T> {
  readonly tag = 'Property';
  key: K;
  schema: Schema<T>;

  constructor(key: K, schema: Schema<T>) {
    this.key = key;
    this.schema = schema;
  }
}

/**
 * Define an optional property on an object. Meaning the key can be omited
 * but given its present the value must match the schema.
 * 
 * 
 * @example
 * import { t, Infer } from "typegate"; 
 * 
 * const schema = t.object(
 *     t.optionalProperty('name', t.string),
 * );
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name?: string }
 * 
 * schema.parse({ name: 'Mike' }); // ✅ Success
 * schema.parse({ name: 123 });    // ❌ Failure
 * schema.parse({});               // ✅ Success
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const optionalProperty =  <K extends string, T>(key: K, schema: Schema<T>) => new OptionalProperty(key, schema);

class OptionalProperty<K extends string, T> {
  readonly tag = 'OptionalProperty';
  key: K;
  schema: Schema<T>;

  constructor(key: K, schema: Schema<T>) {
    this.key = key;
    this.schema = schema;
  }
}


/**
 * Make all properties of an object optional.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const person = t.object(
 *   t.property('name', t.string),
 *   t.property('age', t.number),
 * );
 * 
 * const schema = t.partial(person);
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name?: string, age?: number };
 * 
 * schema.parse({ name: 'Bob', age: 2 }); // ✅ Success
 * schema.parse({ name: 'Mike' });        // ✅ Success
 * schema.parse({ age: 24 });             // ✅ Success
 * schema.parse({});                      // ✅ Success
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const partial = <T>(objectSchema: ObjectSchema<T>): ObjectSchema<{ [K in keyof T]?: T[K]}> => {
  const properties = [];

  for (const property of objectSchema.properties) {
    if (property instanceof Property) {
      properties.push(new OptionalProperty(property.key, property.schema));
    } else {
      properties.push(property);
    }
  }

  return new ObjectSchema(properties) as any;
}

/**
 * Make all properties of an object required.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const person = t.object(
 *   t.optionalProperty('name', t.string),
 *   t.optionalProperty('age', t.number),
 * );
 * 
 * const schema = t.required(person);
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name: string, age: number };
 * 
 * schema.parse({ name: 'Bob', age: 24 }); // ✅ Success
 * schema.parse({ name: 'Mike' });          // ❌ Failure
 * schema.parse({ age: 24 });               // ❌ Failure
 * schema.parse({});                        // ❌ Failure
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const required = <T>(objectSchema: ObjectSchema<T>): ObjectSchema<Required<T>> => {
  const properties = [];

  for (const property of objectSchema.properties) {
    if (property instanceof OptionalProperty) {
      properties.push(new Property(property.key, property.schema));
    } else {
      properties.push(property);
    }
  }

  return new ObjectSchema(properties) as any;
}

/**
 * Only keep the mentionned properties of an object.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const person = t.object(
 *   t.property('name', t.string),
 *   t.property('age', t.number),
 * );
 * 
 * const schema = t.pick(person, ['name']);
 * type Schema = Infer<typeof schema>;
 * //     🔎 { name: string };
 * 
 * schema.parse({ name: 'Mike' }); // ✅ Success
 * schema.parse({ age: 24 });      // ❌ Failure
 * schema.parse({});               // ❌ Failure
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const pick = <T, K extends keyof T>(objectSchema: ObjectSchema<T>, keys: K[]): ObjectSchema<Pick<T, K>> => {
  const properties = [];

  for (const property of objectSchema.properties) {
    if (keys.includes(property.key as any)) {
      properties.push(property);
    }
  }

  return new ObjectSchema(properties) as any;
}

/**
 * Remove the mentionned properties of an object.
 * 
 * @example
 * import { t, Infer } from "typegate";
 * 
 * const person = t.object(
 *   t.property('name', t.string),
 *   t.property('age', t.number),
 * );
 * 
 * const schema = t.omit(person, ['name']);
 * type Schema = Infer<typeof schema>;
 * //     🔎 { age: number };
 * 
 * schema.parse({ name: 'mike' }); // ❌ Failure
 * schema.parse({ age: 24 });      // ✅ Success
 * schema.parse({});               // ❌ Failure
 * @see {@link https://www.typegate.dev Need help? Official Guide}
 */
export const omit = <T,K extends keyof T>(objectSchema: ObjectSchema<T>, keys: K[]): ObjectSchema<Omit<T, K>> => {
  const properties = [];

  for (const property of objectSchema.properties) {
    if (!keys.includes(property.key as any)) {
      properties.push(property);
    }
  }

  return new ObjectSchema(properties) as any;

}