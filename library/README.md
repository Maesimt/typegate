<div align="center">
  <img width="400" src="https://gitlab.com/Maesimt/typegate/-/raw/main/docs/images/typegate.png"/><br />
  <h1>Type<b>Gate</b> - Early Access</h1>

<a href="https://badge.fury.io/js/typegate"><img src="https://badge.fury.io/js/typegate.svg" alt="npm version"></a>
<a href="https://gitlab.com/Maesimt/typegate/-/raw/main/license?ref_type=heads"><img alt="license isc" src="https://img.shields.io/badge/License-ISC-blue.svg" /></a> 
<a href="https://gitlab.com/Maesimt/typegate/-/commits/main"><img alt="coverage report" src="https://gitlab.com/Maesimt/typegate/badges/main/coverage.svg" /></a>
<a href="https://gitlab.com/Maesimt/typegate/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/Maesimt/typegate/badges/main/pipeline.svg" /></a>

</div>

Define schemas that perform runtime checks. Infer the Typescript types from those schemas. Error messages and stack traces do not expose any Personally Identifiable Information (PII) or Protected Health Information (PHI). Does not crash TSC inference until objects are very large. Outputs readable human messages (out-of-the-box). This is not a library to validate forms on the frontend. Its main focus is to validate JSON payloads on the backend. Our goal is to reject faulty payloads promptly to minimize system load.

```typescript
import { t, Infer } from "typegate";

const person = t.object(
  t.property('firstname', t.string),
  t.optionalProperty('age', t.number),
  t.property('children', t.list(t.object(
    t.property('name', string),
    t.optionalProperty('favoriteActivity', t.union(t.literal('Basketball'), t.literal('Drawing')))
  )))
);

type Person = Infer<typeof Person>;

const someData: unknown = {
    firstname: 'Someone',
    children: [
      {name: '1st kid'},
      {name: '2nd kid', favoriteActivity: 123}
    ],
};

const result = parser(someData);

if (result.success) {
  const person = result.value;
  person.children[0].favoriteActivity; 
} else {
  // The property at "<root>.children[1].favoriteActivity" 
  // was supposed to be one of "Basketball" | "Drawing" but it was a number.
  console.log(result.error);  
}
```

# Pros

- No input data in the error AST to prevent PII/PHI leaks in logs.

- Fails at first encountered error. 

- Out-of-the-box human-readable messages.

- Minimalist (10% of most popular alternatives).

- Faster at runtime than most popular alternatives.

- Easier on TSC than most popular alternatives.

- No dependencies.