"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VALID_PERSON = void 0;
var Gender;
(function (Gender) {
    Gender["male"] = "male";
    Gender["female"] = "female";
})(Gender || (Gender = {}));
exports.VALID_PERSON = {
    "firstName": "abc",
    "lastName": "def",
    "age": 32,
    "gender": Gender.male,
    "cars": [
        { brand: "Mazda", maxPassengers: 7, numberOfWheels: 4, safetyRating: 5, price: 32700, tireType: "Summer" },
        { brand: "Honda", maxPassengers: 5, numberOfWheels: 4, safetyRating: 2, price: 38230, tireType: "Summer" },
        { brand: "Toyota", maxPassengers: 5, numberOfWheels: 4, safetyRating: 4, price: 35400, tireType: "Summer" },
        { brand: "Ford", maxPassengers: 5, numberOfWheels: 4, safetyRating: 4, price: 49700, tireType: "Winter" },
    ],
    "investmentAccount": [
        { type: 'TFSA', accountNumber: "123_123_132", amount: 34213, registeredSince: new Date("2024-08-04") },
        { type: 'RRSP', accountNumber: "123_123_132", amount: 34213, registeredSince: new Date("2024-08-04"), remainingCapacity: 32000 },
        { type: 'RRSP', accountNumber: "123_123_132", amount: 34213, registeredSince: new Date("2024-08-04"), remainingCapacity: 32000 },
        { type: 'NON-REGISTERED', accountNumber: "123_123_132", amount: 34213 },
    ],
    "hobbies": [
        { type: "VideoGame", name: "Dota 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Counter-Strike Source", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Counter-Strike 1.6", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Counter-Strike GO", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Counter-Strike 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Half-Life", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Half-life 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Portal", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
        { type: "VideoGame", name: "Portal 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{ platformName: "Xbox" }, { platformName: "PC" }] },
    ],
};
