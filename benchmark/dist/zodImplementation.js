"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zodPersons = void 0;
const zod_1 = require("zod");
const sportBase = zod_1.z.object({
    teamSize: zod_1.z.number(),
    outside: zod_1.z.boolean(),
});
const hockey = zod_1.z.intersection(sportBase, zod_1.z.object({ name: zod_1.z.literal('Hockey') }));
const basketBall = zod_1.z.intersection(sportBase, zod_1.z.object({ name: zod_1.z.literal('BasketBall') }));
const golf = zod_1.z.intersection(sportBase, zod_1.z.object({ name: zod_1.z.literal('Golf') }));
const sport = zod_1.z.union([hockey, basketBall, golf]);
const boardgame = zod_1.z.object({
    type: zod_1.z.literal('BoardGame'),
    name: zod_1.z.string(),
    numberOfPlayers: zod_1.z.number(),
    duration: zod_1.z.number(),
    releasedIn: zod_1.z.date(),
    publisher: zod_1.z.string(),
    kickstarted: zod_1.z.boolean(),
});
const xbox = zod_1.z.object({ platformName: zod_1.z.literal('Xbox') });
const playstation = zod_1.z.object({ platformName: zod_1.z.literal('Playstation') });
const nintendo = zod_1.z.object({ platformName: zod_1.z.literal('Nintendo') });
const steam = zod_1.z.object({ platformName: zod_1.z.literal('Steam') });
const pc = zod_1.z.object({ platformName: zod_1.z.literal('PC') });
const platform = zod_1.z.union([xbox, playstation, nintendo, steam, pc]);
const videoGame = zod_1.z.object({
    type: zod_1.z.literal('VideoGame'),
    name: zod_1.z.string(),
    availablePlatforms: zod_1.z.array(platform),
    earlyAccess: zod_1.z.boolean(),
    studio: zod_1.z.string(),
});
const game = zod_1.z.union([boardgame, videoGame]);
const hobby = zod_1.z.union([sport, game]);
const tfsa = zod_1.z.object({
    type: zod_1.z.literal('TFSA'),
    accountNumber: zod_1.z.string(),
    registeredSince: zod_1.z.date(),
    amount: zod_1.z.number(),
});
const rrsp = zod_1.z.object({
    type: zod_1.z.literal('RRSP'),
    accountNumber: zod_1.z.string(),
    registeredSince: zod_1.z.date(),
    amount: zod_1.z.number(),
    remainingCapacity: zod_1.z.number(),
});
const nonRegisteredAccount = zod_1.z.object({
    type: zod_1.z.literal('NON-REGISTERED'),
    accountNumber: zod_1.z.string(),
    amount: zod_1.z.number(),
});
const investmentAccount = zod_1.z.union([tfsa, rrsp, nonRegisteredAccount]);
const baseCar = zod_1.z.object({
    tireType: zod_1.z.union([zod_1.z.literal('Summer'), zod_1.z.literal('Winter')]),
    numberOfWheels: zod_1.z.literal(4),
    maxPassengers: zod_1.z.number(),
    safetyRating: zod_1.z.union([zod_1.z.literal(1), zod_1.z.literal(2), zod_1.z.literal(3), zod_1.z.literal(4), zod_1.z.literal(5)]),
    price: zod_1.z.number(),
});
const mazda = zod_1.z.intersection(baseCar, zod_1.z.object({ brand: zod_1.z.literal('Mazda') }));
const toyota = zod_1.z.intersection(baseCar, zod_1.z.object({ brand: zod_1.z.literal('Toyota') }));
const hyundai = zod_1.z.intersection(baseCar, zod_1.z.object({ brand: zod_1.z.literal('Hyunday') }));
const ford = zod_1.z.intersection(baseCar, zod_1.z.object({ brand: zod_1.z.literal('Ford') }));
const honda = zod_1.z.intersection(baseCar, zod_1.z.object({ brand: zod_1.z.literal('Honda') }));
const car = zod_1.z.union([mazda, toyota, hyundai, ford, honda]);
var Gender;
(function (Gender) {
    Gender["male"] = "male";
    Gender["female"] = "female";
})(Gender || (Gender = {}));
const gender = zod_1.z.nativeEnum(Gender);
const person = zod_1.z.object({
    firstName: zod_1.z.string(),
    lastName: zod_1.z.string(),
    age: zod_1.z.number(),
    gender: gender,
    investmentAccount: zod_1.z.array(investmentAccount),
    hobbies: zod_1.z.array(hobby),
    cars: zod_1.z.array(car),
});
exports.zodPersons = zod_1.z.array(person);
