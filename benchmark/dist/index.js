"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const zodImplementation_1 = require("./zodImplementation");
const typescriptPerson_1 = require("./typescriptPerson");
const typegateImplementation_1 = require("./typegateImplementation");
const suite_1 = require("./suite");
function clone(item, times) {
    const clones = [];
    for (let i = 0; i < times; ++i) {
        clones.push(JSON.parse(JSON.stringify(item)));
    }
    return clones;
}
function zod(data) {
    return () => {
        return zodImplementation_1.zodPersons.safeParse(data);
    };
}
function typegate(data) {
    return () => {
        return typegateImplementation_1.typegatePersons.parse(data);
    };
}
function main() {
    const mySuite = new suite_1.Suite().setRunLimit(1);
    const test_NoError_1_Object = clone(typescriptPerson_1.VALID_PERSON, 1);
    mySuite.addScenario("TypeGate | No Errors | 1 objects", typegate(test_NoError_1_Object));
    mySuite.addScenario("Zod | No Errors | 1 objects", zod(test_NoError_1_Object));
    const test_NoError_10_Object = clone(typescriptPerson_1.VALID_PERSON, 10);
    mySuite.addScenario("TypeGate | No Errors | 10 objects", typegate(test_NoError_10_Object));
    mySuite.addScenario("Zod | No Errors | 10 objects", zod(test_NoError_10_Object));
    const test_NoError_100_Object = clone(typescriptPerson_1.VALID_PERSON, 100);
    mySuite.addScenario("TypeGate | No Errors | 100 objects", typegate(test_NoError_100_Object));
    mySuite.addScenario("Zod | No Errors | 10 objects", zod(test_NoError_10_Object));
    const test_NoError_1_000_Object = clone(typescriptPerson_1.VALID_PERSON, 1000);
    mySuite.addScenario("TypeGate | No Errors | 1_000 objects", typegate(test_NoError_1_000_Object));
    mySuite.addScenario("Zod | No Errors | 10 objects", zod(test_NoError_10_Object));
    const test_NoError_10_000_Object = clone(typescriptPerson_1.VALID_PERSON, 10000);
    mySuite.addScenario("TypeGate | No Errors | 10_000 objects", typegate(test_NoError_10_000_Object));
    mySuite.addScenario("Zod | No Errors | 10 objects", zod(test_NoError_10_Object));
    const test_NoError_100_000_Object = clone(typescriptPerson_1.VALID_PERSON, 100000);
    mySuite.addScenario("TypeGate | No Errors | 100_000 objects", typegate(test_NoError_100_000_Object));
    mySuite.addScenario("Zod | No Errors | 10 objects", zod(test_NoError_10_Object));
    mySuite.start();
    mySuite.logStats();
}
main();
