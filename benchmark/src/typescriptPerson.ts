enum Gender {
    male = "male",
    female = "female",
}

type SportBase = {
    teamSize: number;
    outside: boolean;
};

type Hockey = SportBase & { name: 'Hockey' };

type BasketBall = SportBase & { name: 'BasketBall' };

type Golf = SportBase & { name: 'Golf' };

type Sport = Hockey | BasketBall | Golf;

type BoardGame = {
    type: "BoardGame",
    name: string;
    numberOfPlayers: number;
    duration: number;
    releasedIn: Date;
    publisher: string;
    kickstarted: boolean;
};


type Xbox = { platformName: 'Xbox'};
type Playstation = { platformName: 'Playstation'};
type Nintendo = { platformName: 'Nintendo'};
type Steam = { platformName: 'Steam'};
type PC =  { platformName: 'PC'};

type Platform = Xbox | Playstation | Nintendo | Steam | PC;

type VideoGame = {
    type: "VideoGame",
    name: string;
    availablePlatforms: Platform[],
    earlyAccess: boolean;
    studio: string;
}

type Game = BoardGame | VideoGame;

type Hobby = Sport | Game;

type TFSA = {
    type: 'TFSA'
    accountNumber: string;
    registeredSince: Date;
    amount: number;
}

type RRSP = {
    type: 'RRSP'
    accountNumber: string;
    registeredSince: Date;
    amount: number;
    remainingCapacity: number;
};

type NonRegisteredAccount = {
    type: 'NON-REGISTERED'
    accountNumber: string;
    amount: number;
};

type InvestmentAccount = TFSA | RRSP | NonRegisteredAccount;

type BaseCar = {
    tireType: 'Summer' | 'Winter';
    numberOfWheels: 4,
    maxPassengers: number,
    safetyRating: 1 | 2 | 3 | 4 | 5,
    price: number;
}

type Mazda = BaseCar & { brand: 'Mazda' };
type Toyota = BaseCar & { brand: 'Toyota' };
type Hyunday = BaseCar & { brand: 'Hyunday' };
type Ford = BaseCar & { brand: 'Ford' };
type Honda = BaseCar & { brand: 'Honda' };

type Car = Mazda | Toyota | Hyunday | Ford | Honda;

type Person = {
    firstName: string;
    lastName: string;
    age: number;
    gender: Gender;

    investmentAccount: InvestmentAccount[];

    hobbies: Hobby[]

    cars: Car[]
}

export type TypescriptPersons = Person[];

export const VALID_PERSON: Person = {
    "firstName": "abc",
    "lastName": "def",
    "age": 32,
    "gender": Gender.male,
    "cars": [
        { brand: "Mazda", maxPassengers: 7, numberOfWheels: 4, safetyRating: 5, price: 32_700, tireType: "Summer"},
        { brand: "Honda", maxPassengers: 5, numberOfWheels: 4, safetyRating: 2, price: 38_230, tireType: "Summer"},
        { brand: "Toyota", maxPassengers: 5, numberOfWheels: 4, safetyRating: 4, price: 35_400, tireType: "Summer"},
        { brand: "Ford", maxPassengers: 5, numberOfWheels: 4, safetyRating: 4, price: 49_700, tireType: "Winter"},
    ],
    "investmentAccount": [
        { type: 'TFSA', accountNumber: "123_123_132", amount: 34_213, registeredSince: new Date("2024-08-04")},
        { type: 'RRSP', accountNumber: "123_123_132", amount: 34_213, registeredSince: new Date("2024-08-04"), remainingCapacity: 32_000},
        { type: 'RRSP', accountNumber: "123_123_132", amount: 34_213, registeredSince: new Date("2024-08-04"), remainingCapacity: 32_000},
        { type: 'NON-REGISTERED', accountNumber: "123_123_132", amount: 34_213 },
    ],
    "hobbies": [
        { type: "VideoGame", name: "Dota 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Counter-Strike Source", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Counter-Strike 1.6", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Counter-Strike GO", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Counter-Strike 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Half-Life", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Half-life 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Portal", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
        { type: "VideoGame", name: "Portal 2", studio: "Valve", earlyAccess: false, availablePlatforms: [{platformName: "Xbox"}, {platformName: "PC"}] },
    ],
}