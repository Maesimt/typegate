import { z } from 'zod';
import { IsTrue } from 'typegate';

import { TypescriptPersons } from './typescriptPerson';

const sportBase = z.object({
    teamSize: z.number(),
    outside: z.boolean(),
});

const hockey = z.intersection(sportBase, z.object({ name: z.literal('Hockey') }));
const basketBall = z.intersection(sportBase, z.object({ name: z.literal('BasketBall') }));
const golf = z.intersection(sportBase, z.object({ name: z.literal('Golf') }));

const sport = z.union([hockey, basketBall, golf]);


const boardgame = z.object({
    type: z.literal('BoardGame'),
    name: z.string(),
    numberOfPlayers: z.number(),
    duration: z.number(),
    releasedIn: z.date(),
    publisher: z.string(),
    kickstarted: z.boolean(),
});

const xbox = z.object({platformName: z.literal('Xbox')});
const playstation = z.object({platformName: z.literal('Playstation')});
const nintendo = z.object({platformName: z.literal('Nintendo')});
const steam = z.object({platformName: z.literal('Steam')});
const pc = z.object({platformName: z.literal('PC')});

const platform = z.union([xbox, playstation,nintendo,steam,pc]);

const videoGame = z.object({
    type: z.literal('VideoGame'),
    name: z.string(),
    availablePlatforms: z.array(platform),
    earlyAccess: z.boolean(),
    studio: z.string(),
});

const game = z.union([boardgame, videoGame]);

const hobby = z.union([sport, game]);

const tfsa = z.object({
    type: z.literal('TFSA'),
    accountNumber: z.string(),
    registeredSince: z.date(),
    amount: z.number(),
});

const rrsp = z.object({
    type: z.literal('RRSP'),
    accountNumber: z.string(),
    registeredSince: z.date(),
    amount: z.number(),
    remainingCapacity: z.number(),
});

const nonRegisteredAccount = z.object({
    type: z.literal('NON-REGISTERED'),
    accountNumber: z.string(),
    amount: z.number(),
});

const investmentAccount = z.union([tfsa, rrsp, nonRegisteredAccount])

const baseCar = z.object({
    tireType: z.union([z.literal('Summer'),z.literal('Winter')]),
    numberOfWheels: z.literal(4),
    maxPassengers: z.number(),
    safetyRating: z.union([z.literal(1),z.literal(2),z.literal(3),z.literal(4),z.literal(5)]),
    price:  z.number(),
});


const mazda = z.intersection(baseCar, z.object({brand: z.literal('Mazda')}));
const toyota = z.intersection(baseCar, z.object({brand: z.literal('Toyota')}));
const hyundai = z.intersection(baseCar, z.object({brand: z.literal('Hyunday')}));
const ford = z.intersection(baseCar, z.object({brand: z.literal('Ford')}));
const honda = z.intersection(baseCar, z.object({brand: z.literal('Honda')}));

const car = z.union([mazda, toyota, hyundai, ford, honda]);

enum Gender {
    male = "male",
    female = "female",
}

const gender = z.nativeEnum(Gender);


const person = z.object({
    firstName: z.string(),
    lastName: z.string(),
    age: z.number(),
    gender: gender,

    investmentAccount: z.array(investmentAccount),

    hobbies: z.array(hobby),

    cars: z.array(car),
});

export const zodPersons = z.array(person);
type ZodPersons = z.infer<typeof zodPersons>;

type Check_1 = IsTrue<ZodPersons extends TypescriptPersons ? true : false>;
type Check_2 = IsTrue<TypescriptPersons extends ZodPersons ? true : false>; 