import { t, IsTrue, Infer } from 'typegate';

import { TypescriptPersons } from './typescriptPerson';

const sportBase = t.object(
    t.property("teamSize", t.number),
    t.property("outside", t.boolean),
);

const hockey = t.intersection(sportBase, t.object(t.property("name", t.literal('Hockey'))));
const basketBall = t.intersection(sportBase, t.object(t.property("name", t.literal('BasketBall') )));
const golf = t.intersection(sportBase, t.object(t.property("name", t.literal('Golf') )));

const sport = t.union(hockey, basketBall, golf);


const boardgame = t.object(
    t.property("type", t.literal('BoardGame')),
    t.property("name", t.string),
    t.property("numberOfPlayers", t.number),
    t.property("duration", t.number),
    t.property("releasedIn", t.noCheck<Date>()), // TODO: CustomParser ISO-8601 ?
    t.property("publisher", t.string),
    t.property("kickstarted", t.boolean),
);

const xbox = t.object(t.property("platformName", t.literal('Xbox')));
const playstation = t.object(t.property("platformName", t.literal('Playstation')));
const nintendo = t.object(t.property("platformName", t.literal('Nintendo')));
const steam = t.object(t.property("platformName", t.literal('Steam')));
const pc = t.object(t.property("platformName", t.literal('PC')));

const platform = t.union(xbox, playstation, nintendo, steam, pc);

const videoGame = t.object(
    t.property("type", t.literal('VideoGame')),
    t.property("name", t.string),
    t.property("availablePlatforms", t.array(platform)),
    t.property("earlyAccess", t.boolean),
    t.property("studio", t.string),
);

const game = t.union(boardgame, videoGame);

const hobby = t.union(sport, game);

const tfsa = t.object(
    t.property("type", t.literal('TFSA')),
    t.property("accountNumber", t.string),
    t.property("registeredSince", t.noCheck<Date>()),
    t.property("amount", t.number),
);

const rrsp = t.object(
    t.property("type", t.literal('RRSP')),
    t.property("accountNumber", t.string),
    t.property("registeredSince", t.noCheck<Date>()),
    t.property("amount", t.number),
    t.property("remainingCapacity", t.number),
);

const nonRegisteredAccount = t.object(
    t.property("type", t.literal('NON-REGISTERED')),
    t.property("accountNumber", t.string),
    t.property("amount", t.number),
);

const investmentAccount = t.union(tfsa, rrsp, nonRegisteredAccount)

const baseCar = t.object(
    t.property("tireType", t.union(t.literal('Summer'),t.literal('Winter'))),
    t.property("numberOfWheels", t.literal(4)),
    t.property("maxPassengers", t.number),
    t.property("safetyRating", t.union(t.literal(1),t.literal(2),t.literal(3),t.literal(4),t.literal(5))),
    t.property("price",  t.number),
);


const mazda = t.intersection(baseCar, t.object(t.property("brand", t.literal('Mazda'))));
const toyota = t.intersection(baseCar, t.object(t.property("brand", t.literal('Toyota'))));
const hyundai = t.intersection(baseCar, t.object(t.property("brand", t.literal('Hyunday'))));
const ford = t.intersection(baseCar, t.object(t.property("brand", t.literal('Ford'))));
const honda = t.intersection(baseCar, t.object(t.property("brand", t.literal('Honda'))));

const car = t.union(mazda, toyota, hyundai, ford, honda);

enum Gender {
    male = "male",
    female = "female",
}

const gender = t.enum(Gender);


const person = t.object(
    t.property("firstName", t.string),
    t.property("lastName", t.string),
    t.property("age", t.number),
    t.property("gender", gender),
    t.property("investmentAccount", t.array(investmentAccount)),
    t.property("hobbies", t.array(hobby)),
    t.property("cars", t.array(car)),
);

export const typegatePersons = t.array(person);
type TypeGatePersons = Infer<typeof typegatePersons>;

type Check_1 = IsTrue<TypeGatePersons extends TypescriptPersons ? true : false>;
type Check_2 = IsTrue<TypescriptPersons extends TypeGatePersons ? true : false>; 