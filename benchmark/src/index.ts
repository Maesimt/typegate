import { zodPersons } from "./zodImplementation";
import { VALID_PERSON } from "./typescriptPerson";
import { typegatePersons } from "./typegateImplementation";
import { Suite } from './suite';

function clone<T>(item: T, times: number): T[] {
    const clones = [];
    for(let i = 0; i < times; ++i) {
        clones.push(JSON.parse(JSON.stringify(item)));
    }
    return clones;
}

function zod (data: unknown) {
    return () => {
        return zodPersons.safeParse(data);
    }
}

function typegate (data: unknown) {
    return () => {
        return typegatePersons.parse(data);
    }
}

function main() {
    const mySuite = new Suite().setRunLimit(1);


    // TODO: Multi-thread

    const totalObjects = [1, 10, 100, 1_000, 10_000, 100_000];

    for (const totalObject of totalObjects) {
        const test_data = clone(VALID_PERSON, totalObject);
        mySuite.addScenario(`TypeGate | 0 Errors | ${totalObject} objects`, typegate(test_data));
        mySuite.addScenario(`Zod | 0 Errors | ${totalObject} objects`, zod(test_data));
    }

    for (const totalObject of totalObjects) {
        for (let percent = 1; percent < 100; percent++){
            // TODO: 
            // - Calculate 1% equals to which index in the total.
            // - Insert a baddy at that index.
            const test_data = clone(VALID_PERSON, totalObject);
            mySuite.addScenario(`TypeGate | 0 Errors | ${totalObject} objects`, typegate(test_data));
            mySuite.addScenario(`Zod | 0 Errors | ${totalObject} objects`, zod(test_data));
        }

    }
    
    mySuite.start();
    mySuite.logStats();

    // Generate jpeg with vega
    // https://stackoverflow.com/questions/44543729/how-to-render-a-graph-as-image-in-node
    // Or with vocs we plot it with a lib.
}

main();