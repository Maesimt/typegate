
type Scenario = {
    name: string, // Zod | 1 error at beginning | 1000 objects
    testFn: () => void,
    runs: number[], //ms
}



export class Suite {
    private scenarios: Record<string, Scenario> = {};
    private started = false;
    private done = false;
    private pool: string[] = [];
    private runLimit: number = 1;

    setRunLimit(times: number): Suite {
        this.runLimit = times;
        return this;
    }


    addScenario(name: string, testFn: () => void): Suite {
        this.scenarios[name] = {
            name,
            testFn,
            runs: [],
        };

        return this;
    }

    logStats() {
        if (this.done !== true) {
            return;
        }

        const scenarios = Object.values(this.scenarios);
        for (const scenario of scenarios) {
            let sum = 0;
            for(const run of scenario.runs) {
                sum += run;
            }
            const mean = sum / scenario.runs.length;
            console.log(`${scenario.name} mean: ${mean}ms`);
        }
    }

    start() {
        if (this.started) {
            return;
        }
        
        this.started = true;
        this.pool = Object.keys(this.scenarios);

        while (this.pool.length > 0) {
            const pulledName = Suite.draw(this.pool);

            const scenario = this.scenarios[pulledName];

            if (scenario.runs.length >= this.runLimit) {
                const pulledNameIndex = this.pool.findIndex(e => e === pulledName);
                if (pulledNameIndex === -1) {
                    // TODO: TBD
                }
                this.pool.splice(pulledNameIndex, 1);
                continue;
            }

            console.log(`Start the run #${scenario.runs.length} for scenario "${scenario.name}".`)

            const startTime = performance.now();

            scenario.testFn();

            const endTime = performance.now();
            const runTime = endTime - startTime;

            scenario.runs.push(runTime);
            console.log(`          run #${scenario.runs.length - 1} for scenario "${scenario.name}" took ${runTime}ms.`)
        }

        this.done = true;
    }

    static draw(pool: string[]) {
        const index = Math.floor(Math.random() * pool.length);
        return pool[index];
    }
}